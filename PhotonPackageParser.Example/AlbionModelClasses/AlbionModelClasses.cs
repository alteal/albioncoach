﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonPackageParser.Program

{
    class AlbionModelClasses
    {
    }

    public class GuildUpdate
    {

            public Guid GuildGuid; // Guild GUID // Key0
            public Guid[] PlayerGuid; // All players GUID  // Key1
            public string[] Playernames; // All playernames  // Key2
            public bool[] PlayerOnline; // ONLINE/OFFLINE ?  // Key3
            public Int64[] LastSeen; // Gametimestamp  // Key4
            public byte[] ask; // ?  // Key5
            public int[] asl;  // ?  // Key6
            public int[] asm; // ?  // Key7
            public int[] asn; // ? // Key8

            public byte aso; // ?
            public long asp; // ?
            public long asq; //
            public bool asr; // Sometimes not present
            public long ass; // Sometimes not present

            public string ast; // Guildname  // Key14
            public string asu; // Alliancename  // Key15
            public Guid asv; // Alliance GUID ?  // Key16
            public bool asw; //Sometimes not present
            public Dictionary<int, long> asx; // ? KEY18

            public int asy;// ?
            public int asz;// ?
            public int as0;// ?
            public int as1;// ?
            public int as2;// ?
            public float as3;// ?
            public float as4;// ?
            public long as5;// ?
            public int as6;// ?
    }

    public class TakeSilver
    {
      /*  public void FixPrices()
        {
            YieldPreTax = YieldPreTax / 10000;
            ClusterTax = ClusterTax / 10000;
            GuildTax = GuildTax / 10000;
        }
      */
        public Int32 ObjectID;
        public Int32 TimeStamp;
        public Int32 TargetEntityId;
        public Int64 YieldPreTax;
        public Int64 ClusterTax;
        public Int64 GuildTax;
        public bool PremiumBonus;
        public bool ClusterBonus;
    }

    public class Sellorder
    {
        public void FixPrices()
        {
            UnitPriceSilver = UnitPriceSilver / 10000;
            TotalPriceSilver = TotalPriceSilver / 10000;
        }
        public Int64 Id { get; set; }
        public Int64 UnitPriceSilver { get; set; }
        public Int64 TotalPriceSilver { get; set; }
        public Int64 Amount { get; set; }
        public Int64 Tier { get; set; }
        public bool IsFinished { get; set; }
        public string AuctionType { get; set; }
        public bool HasBuyerFetched { get; set; }
        public bool HasSellerFetched { get; set; }
        public string SellerCharacterId { get; set; }
        public string SellerName { get; set; }
        public string BuyerCharacterId { get; set; }
        public string BuyerName { get; set; }
        public string ItemTypeId { get; set; }
        public string ItemGroupTypeId { get; set; }
        public Int64 EnchantmentLevel { get; set; }
        public Int64 QualityLevel { get; set; }
        public string Expires { get; set; }
    }

   

    public class Buyorder
    {
        public void FixPrices()
        {
            UnitPriceSilver = UnitPriceSilver / 10000;
            TotalPriceSilver = TotalPriceSilver / 10000;
        }
        public Int64 Id { get; set; }
        public Int64 UnitPriceSilver { get; set; }
        public Int64 TotalPriceSilver { get; set; }
        public Int64 Amount { get; set; }
        public Int64 Tier { get; set; }
        public bool IsFinished { get; set; }
        public string AuctionType { get; set; }
        public bool HasBuyerFetched { get; set; }
        public bool HasSellerFetched { get; set; }
        public string SellerCharacterId { get; set; }
        public string SellerName { get; set; }
        public string BuyerCharacterId { get; set; }
        public string BuyerName { get; set; }
        public string ItemTypeId { get; set; }
        public string ItemGroupTypeId { get; set; }
        public Int64 EnchantmentLevel { get; set; }
        public Int64 QualityLevel { get; set; }
        public string Expires { get; set; }
    }



}
