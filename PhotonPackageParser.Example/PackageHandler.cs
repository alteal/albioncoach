﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Web;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using PhotonPackageParser.Program;

using System.Collections;
using System.Linq;
using System.IO;




using System.Runtime.Serialization.Formatters.Binary;

namespace AlbionPacketParser
{
    

    public class PackageHandler : IPhotonPackageHandler
    {

        Overlay overlayForm;
        double TotalSilver;
        public int ObjectID;
        public BinaryFormatter formatter = new BinaryFormatter();

        bool saved = false;

     public   StreamWriter _streamWriter =new StreamWriter(string.Format("text-{0:yyyy-MM-dd_hh-mm-ss-tt}.txt",DateTime.Now));

        //public PackageHandler(Overlay form)
        //{
        //    overlayForm = form;
        //}
    

        public void Closestream()
        {
            _streamWriter.Close();
            _streamWriter.Dispose();

            
        }

        public void WriteStream(string text)
        {
            _streamWriter.WriteLine(text);
            Console.WriteLine(text);
        }


        // Json functions.
        // JSON STRING ARRAY to Objects
        // Used when the string contains multiple JSON objects
        public List<T> DeserializeJsontoObjectList<T>(string Json)
        {
            JavaScriptSerializer JavaScriptSerializer = new JavaScriptSerializer();
            return JavaScriptSerializer.Deserialize<List<T>>(Json);

            

            
        }

        // JSON STRING to Object 
        // Used when the string contains one JSON object
        public object DeserializeJsontoObject<T>(string Json)
        {
            JavaScriptSerializer JavaScriptSerializer = new JavaScriptSerializer();
            T obj = JavaScriptSerializer.Deserialize<T>(Json);
            return obj;

        }

        
        public void WriteDict(Dictionary<byte, object> parameters)
        {

            
            // Create a FileStream that will write data to file.
            FileStream writerFileStream = new FileStream("TestFile.dat", FileMode.Create, FileAccess.Write);
            this.formatter.Serialize(writerFileStream, parameters);           
            writerFileStream.Close();
        }
        public Dictionary<byte,object> ReadDict (string filename)
        {
            FileStream readerFileStream = new FileStream(filename,FileMode.Open, FileAccess.Read);

            Dictionary<byte,object> dict = (Dictionary<byte, object>)formatter.Deserialize(readerFileStream);
            // Close the readerFileStream when we are done
            readerFileStream.Close();

            return dict;
        }

        

        public void UpdateOverlay(string type, int Eventcode, int Operationcode, Dictionary<byte, object> parameters)
        {
            var OverlayForm = Program.GetOverlay;
            //var OverlayForm = Application.OpenForms.OfType<Overlay>().Single();

            try
            {
                OverlayForm.Invoke((Action)(() => { OverlayForm.Debug(type, Eventcode, Operationcode, parameters); }));
            } catch
            {
                Console.WriteLine("Failed to Invoke action to overlay: " + type);    
            }
        }
       


        public void DebugUI(string type, int Eventcode, int Operationcode, Dictionary<byte, object> parameters)
        {
                //var UIForm = Application.OpenForms.OfType<UserInterface>().Single();
                //UIForm.Invoke((Action)(() => { UIForm.Debug(type, Eventcode, Operationcode, parameters); } ) );
  
        }

        public void LogPlayerSkillUse()
        {

        }

        public void DeterminePlayers()
        {

        }




        /////////////////////////////////////////////////////////////START/////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////Event & Operation Handlers/////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////START/////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////START/////////////////////////////////////////////////////////////////////////////////
         
        
        /// <summary>
        /// Function called by the Packet-Parser to handle the Events that are invoked from the Albion-Online Game-Server.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="parameters"></param>
        public void OnEvent(byte code, Dictionary<byte, object> parameters)
        {

            

            if (!parameters.TryGetValue(252, out var value))
            { return; }
            int Eventcode = int.Parse(parameters[252].ToString());

            try
            {
                if (!(EventCodes.EventAccept.Contains(EventCodes.EventToString[Eventcode])))
                { return; }
            } catch 
            {
                Console.WriteLine("Exception: Event doesn't exist");
            }

            

            UpdateOverlay("Event", Eventcode, 0, parameters);
            //Eventhandler(Eventcode, parameters);
            //DebugUI("Event", Eventcode, 0, parameters);
        }
        /// <summary>
        /// Function called by the Packet-Parser to handle the responses that are send from the Albion-Online Game-Server.
        /// </summary>
        /// <param name="operationCode"></param>
        /// <param name="returnCode"></param>
        /// <param name="parameters"></param>
        /// 

        public void OnResponse(byte operationCode, short returnCode, Dictionary<byte, object> parameters)
        {
            //if (!parameters.TryGetValue(253, out var value))
            //{ return; }
            //int OperationCode = int.Parse(parameters[253].ToString());
            //if (!(OperationCodes.OperationAccept.Contains(OperationCodes.OperationToString[OperationCode])))
            //{ return; }

            //DebugUI("Response",0, OperationCode, parameters);

        }

       // public void Write()
      ////  {
      //      Program.UIForm.label1.Text = "Response";
      //  }
        /// <summary>
        /// Function called by the Packet-Parser to handle the Requests sent from the Albion-Online Game-Client.
        /// </summary>
        /// <param name="operationCode"></param>
        /// <param name="parameters"></param>
        public void OnRequest(byte operationCode, Dictionary<byte, object> parameters)
        {

            //if (!parameters.TryGetValue(253, out var value))
            //{ return; }
            //int OperationCode = int.Parse(parameters[253].ToString());
            //if (!(OperationCodes.OperationAccept.Contains(OperationCodes.OperationToString[OperationCode])))
            //{ return; }

            //DebugUI("Request", 0, OperationCode, parameters);

        }
        //////////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////Event & Operation Handlers/////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////////////////////



        /////////////////////////////////////////////////////////////START/////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////Packet Handling///////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////START/////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////START/////////////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Handles Events that are invoked by the Albion-Online Game-Server
        /// </summary>
        /// <param name="OperationCode"></param>
        /// <param name="parameters"></param>

        private void Eventhandler(int Eventcode, Dictionary<byte, object> parameters)
        {
            //Console.WriteLine("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode);

            // Create the List of classes

            EventCodes.EventCodes_Enum Code = (EventCodes.EventCodes_Enum)Eventcode;
            //var UIForm = Application.OpenForms.OfType<UserInterface>().Single();
            // Takes the object coming from the packet, and convert it to a list of the MARKETREQUEST model class
            string[] StringArray;

            switch (Code)
            {
                case EventCodes.EventCodes_Enum.NewOrbObject:

                    
                    //if (UIForm.closestream) { Closestream(); return; }

                    WriteStream("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode);

                    foreach (KeyValuePair<byte, object> kvp in parameters)
                    {
                        WriteStream("Key = " + kvp.Key + ", Value = " + kvp.Value);
                    }
                    DebugUI("Event", Eventcode, 0, parameters);


                    break;

                case EventCodes.EventCodes_Enum.ClaimOrbStart:
                    //if (UIForm.closestream) { Closestream(); return; }

                    WriteStream("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode);

                    foreach (KeyValuePair<byte, object> kvp in parameters)
                    {
                        WriteStream("Key = " + kvp.Key + ", Value = " + kvp.Value);
                    }
                    DebugUI("Event", Eventcode, 0, parameters);
                    break;

                case EventCodes.EventCodes_Enum.ClaimOrbFinished:
                    //if (UIForm.closestream) { Closestream(); return; }

                    WriteStream("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode);

                    foreach (KeyValuePair<byte, object> kvp in parameters)
                    {
                        WriteStream("Key = " + kvp.Key + ", Value = " + kvp.Value);
                    }
                    DebugUI("Event", Eventcode, 0, parameters);
                    break;

                case EventCodes.EventCodes_Enum.ClaimOrbCancel:
                    //if (UIForm.closestream) { Closestream(); return; }

                    WriteStream("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode);

                    foreach (KeyValuePair<byte, object> kvp in parameters)
                    {
                        WriteStream("Key = " + kvp.Key + ", Value = " + kvp.Value);
                    }
                    DebugUI("Event", Eventcode, 0, parameters);
                    break;

                case EventCodes.EventCodes_Enum.OrbUpdate:
                    //if (UIForm.closestream) { Closestream(); return; }

                    WriteStream("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode);

                    foreach (KeyValuePair<byte, object> kvp in parameters)
                    {
                        WriteStream("Key = " + kvp.Key + ", Value = " + kvp.Value);
                    }
                    DebugUI("Event", Eventcode, 0, parameters);
                    break;

                case EventCodes.EventCodes_Enum.OrbClaimed:
                    //if (UIForm.closestream) { Closestream(); return; }

                    WriteStream("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode);

                    foreach (KeyValuePair<byte, object> kvp in parameters)
                    {
                        WriteStream("Key = " + kvp.Key + ", Value = " + kvp.Value);
                    }
                    DebugUI("Event", Eventcode, 0, parameters);
                    break;





                // Health Update
                case EventCodes.EventCodes_Enum.HealthUpdate:

                    //"HealthUpdate", // Damage   ObjectID,GameTimeStamp,HealthChange,NewHealthValue,EffectType,EffectOrigin,CauserId,CausingSpellType;
                    break;

                // New character, A new character has entered your range.
                case EventCodes.EventCodes_Enum.NewCharacter:
                    break;
                // Somebody has taken up silver.

                case EventCodes.EventCodes_Enum.TakeSilver:

                    /*
                                        TakeSilver TakenSilver = new TakeSilver();
                                        TakenSilver.ObjectID = (Int32)parameters[0];
                                        TakenSilver.TimeStamp = (Int32)parameters[1];
                                        TakenSilver.TargetEntityId = (Int32)parameters[2];
                                        TakenSilver.YieldPreTax = (long)parameters[3];
                                        TakenSilver.ClusterTax = (long)parameters[4];
                                        TakenSilver.GuildTax = (long)parameters[5];
                                         bool.TryParse(parameters[6].ToString(), out TakenSilver.PremiumBonus);
                                        TakenSilver.ClusterBonus = (bool)parameters[7];
                    */
                    /*
                     * Event:TakeSilver  48
                        Key = 0,  obj id
                        Key = 1,  timestamp
                        Key = 2,  target entity id
                        Key = 3,  yieldpretax 
                        Key = 4,  cluster tax
                        Key = 5,  guild tax
                        Key = 6,  premium bonus
                        Key = 7,  cluster bonus
                        Key = 252 eventcode
                     * 
                     */

                    /*
                    if (TakenSilver.ObjectID==ObjectID)
                    {
                        double Silvertaken = (Convert.ToDouble(TakenSilver.YieldPreTax - (TakenSilver.ClusterTax + TakenSilver.GuildTax))) / 10000;
                        TotalSilver = TotalSilver + Silvertaken;
                    }
                    

                    // Debug
                    Console.WriteLine("Total Silver Picked UP : " +TotalSilver);
                    */
                    break;

                /*
                                case EventCodes.EventCodes_Enum.GuildUpdate:

                                    if (!parameters.TryGetValue(1, out object value))
                                    { return; }

                                    byte[][] GuidAraay =  (byte[][])parameters[1];

                                    List<byte[]> GuidList = GuidAraay.Cast<byte[]>().ToList();

                                    //Guid[] _Guid = new Guid(array[0]);
                                    //string[] Key2 =  (string[])parameters[2];

                                    if (!parameters.TryGetValue(2, out object value2))
                                    { return; }

                                    string[] PlayernamesArray = (string[])parameters[2];



                                    for (int i=0; i < GuidList.Count; i++ )
                                    {
                                        //Console.WriteLine(id[i]);
                                        Guid _Guid = new Guid(GuidList[i]);
                                        Console.WriteLine("GUID" + i + "  " + _Guid.ToString() + " Player:  "+ PlayernamesArray[i]);

                                       //Console.WriteLine("GUID" + i + "  " + _Guid.ToString());

                                    }
                                    break;
                */









                case EventCodes.EventCodes_Enum.GuildPlayerUpdated:

                    break;
                default:
                    break;
                
            }
        }
        /// <summary>
        /// Handles Responses coming from the Albion-Online Game-Server
        /// </summary>
        /// <param name="OperationCode"></param>
        /// <param name="parameters"></param>

        private void ResponseHandler(int OperationCode, Dictionary<byte, object> parameters)
        {

            // Create the List of Marketrequest classes
            List<Sellorder> SellOrders = new List<Sellorder>();
            List<Sellorder> BuyOrders = new List<Sellorder>();

            // Takes the object coming from the packet, and convert it to a list of the MARKETREQUEST model class
            string[] StringArray;


            //Console.WriteLine("Response: " + OperationCodes.OperationToString[OperationCode] + "  " + OperationCode);

            OperationCodes.OperationCodes_Enum Code = (OperationCodes.OperationCodes_Enum)OperationCode;



            switch (Code)
            {
                // You have joined the Area
                case OperationCodes.OperationCodes_Enum.Join:



                    
                    ObjectID = int.Parse(parameters[0].ToString());
                    Console.WriteLine("Your ObjectID = " + ObjectID);
                    break;
                
                // Somebody picked up silver
                case OperationCodes.OperationCodes_Enum.TakeSilver:
                    //"TakeSilver", // Pickup Silver?

                    break;
                
                    // Auctionhouse Sellorders
                case OperationCodes.OperationCodes_Enum.AuctionGetOffers:
                    //"AuctionGetOffers", // Get Sell orders Contains JSON STRING when it comes back as a RESPONSE

                    // Empty the list, so index starts at 0 again.
                    SellOrders.Clear();

                    StringArray = (string[])parameters[0];
                    for (int i = 0; i < StringArray.Length; i++)
                    {

                        // Add the Deserialised String to the ModelClass
                        SellOrders.Add((Sellorder)DeserializeJsontoObject<Sellorder>(StringArray[i]));
                        // Fix the prices. Values are x 10.000 Sent from Albion Server
                        SellOrders[i].FixPrices();

                        // Debug.
                        //Console.WriteLine(SellOrders[i].SellerName + "   " + SellOrders[i].UnitPriceSilver);
                    }
                    break;

                    // AuctionHouse Buyorders
                case OperationCodes.OperationCodes_Enum.AuctionGetRequests:
                    //"AuctionGetRequests", // Get buy orders Contains JSON STRING when it comes back as a RESPONSE

                    // Empty the list, so index starts at 0 again.
                    BuyOrders.Clear();


                    StringArray = (string[])parameters[0];
                    Console.WriteLine(StringArray[0]);
                    for (int i = 0; i < StringArray.Length; i++)
                    {
                        
                       
                        // Add the Deserialised String to the ModelClass
                        BuyOrders.Add((Sellorder)DeserializeJsontoObject<Sellorder>(StringArray[i]));
                        // Fix the prices. Values are x 10.000 Sent from Albion Server
                        BuyOrders[i].FixPrices();

                        // Debug.
                        Console.WriteLine(BuyOrders[i].BuyerName + "   " + BuyOrders[i].UnitPriceSilver);
                        
                    }

                    break;

                case OperationCodes.OperationCodes_Enum.TrackAchievements:


                    Byte[][] key0 = (Byte[][])parameters[0];


                    Int32[] key1 = (Int32[])parameters[1];

                    for (int i = 0; i < key0.GetLength(0);i++)
                    {
                       Console.WriteLine(BitConverter.ToString(key0[i]).Replace("-", string.Empty));
                    }

                    for (int k = 0; k < key1.Length; k++)
                    {
                        Console.WriteLine(key1[k]);
                    }
                    break;

                case OperationCodes.OperationCodes_Enum.QueryGuildPlayerStats:


                    break;


                default:

                    break;

                    
                
            }

                    
            
        }

        /// <summary>
        /// Handles REQUESTS made by the Albion-Online Game Client
        /// </summary>
        /// <param name="OperationCode"></param>
        /// <param name="parameters"></param>
        private void RequestHandler(int OperationCode, Dictionary<byte, object> parameters)
        {
            //Console.WriteLine("Request: " + OperationCodes.OperationToString[OperationCode] + "  " + OperationCode);

            OperationCodes.OperationCodes_Enum Code = (OperationCodes.OperationCodes_Enum)OperationCode;


            switch (Code)
            {
                // Move Request
                case OperationCodes.OperationCodes_Enum.Move:

                    Single[] key1 = (Single[])parameters[1]; // Coordinates to where.
                    Single[] key3 = (Single[])parameters[1];

                    for(int i = 0; i < key1.Length; i++)
                    {
                        Console.WriteLine(key1[i]);
                    }

                    for (int i = 0; i < key3.Length; i++)
                    {
                        Console.WriteLine(key3[i]);
                    }

                break;

                case OperationCodes.OperationCodes_Enum.TrackAchievements:

                    break;


                case OperationCodes.OperationCodes_Enum.QueryGuildPlayerStats:


                    break;
                default:
                    // Not needed
                break;

                
            }

                    
        }
        //////////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////Packet Handling//////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////END//////////////////////////////////////////////////////////////////////////////////




    }
}
