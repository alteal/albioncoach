﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace AlbionPacketParser
{
    public class Settings
    {
        public int? NetworkDeviceIndex = null;
        public Point? FriendlyPlayerContainerPosition = null;
        public Point? EnemyPlayerContainerPosition = null;
        public bool LockPositions;

        public void SaveNetworkDeviceIndex(int index)
        {
            NetworkDeviceIndex = index;
            SaveSettings();
        }

        public void SaveLockPositions(bool locked)
        {
            LockPositions = locked;
            SaveSettings();
        }

        public void SaveEnemyPlayerContainerPosition(Point position)
        {
            EnemyPlayerContainerPosition = position;
            SaveSettings();
        }

        public void SaveFriendlyPlayerContainerPosition(Point position)
        {
            FriendlyPlayerContainerPosition = position;
            SaveSettings();
        }

        public int? TryParseNullable(string value)
        {
            int outValue;
            return int.TryParse(value, out outValue) ? (int?)outValue : null;
        }

        public void ParseErrorMessage(string command)
        {
            Console.WriteLine("Could not parse argument for command \"" + command + "\"");
        }

        public void LoadLockPositions(string command, string[] arguments)
        {
            if (command == "LockPositions")
            {
                if (arguments.Count() < 1)
                {
                    ParseErrorMessage(command);
                    return;
                }

                if (arguments[0] == "True")
                {
                    LockPositions = true;
                }
            }
        }

        public void LoadFriendlyPlayerContainerPosition(string command, string[] arguments)
        {
            if (command == "FriendlyContainerPos")
            {
                if (arguments.Count() < 2)
                {
                    ParseErrorMessage(command);
                    return;
                }

                int? x = TryParseNullable(arguments[0]);
                int? y = TryParseNullable(arguments[1]);

                if (x == null || y == null)
                {
                    ParseErrorMessage(command);
                } else
                {
                    FriendlyPlayerContainerPosition = new Point((int)x, (int)y);
                }
            }
        }

        public void LoadEnemyPlayerContainerPosition(string command, string[] arguments)
        {
            if (command == "EnemyContainerPos")
            {
                if (arguments.Count() < 2)
                {
                    ParseErrorMessage(command);
                    return;
                }

                int? x = TryParseNullable(arguments[0]);
                int? y = TryParseNullable(arguments[1]);

                if (x == null || y == null)
                {
                    ParseErrorMessage(command);
                } else
                {
                    EnemyPlayerContainerPosition = new Point((int)x, (int)y);
                }
            }
        }

        public void LoadNetworkDevice(string command, string[] arguments)
        {
            if (command == "Device")
            {
                if (arguments.Count() < 1)
                {
                    ParseErrorMessage(command);
                    return;
                }

                int deviceIndex;

                Console.WriteLine("LoadNetworkDeviceArg: " + arguments[0]);

                if (Int32.TryParse(arguments[0], out deviceIndex))
                {
                    NetworkDeviceIndex = deviceIndex;
                }
                else
                {
                    ParseErrorMessage(command);
                }
            }
        }

        public void LoadSettings()
        {
            if (File.Exists("settings.ini"))
            {
                string[] lines = File.ReadAllLines("settings.ini");

                for (int i = 0; i <= lines.Count() - 1; i ++)
                {
                    string[] lineData = lines[i].Split(':');
                    string command = lineData[0].Trim(' ');
                    string[] arguments = lineData[1].Trim().Split(' ');

                    LoadNetworkDevice(command, arguments);
                    LoadEnemyPlayerContainerPosition(command, arguments);
                    LoadFriendlyPlayerContainerPosition(command, arguments);
                    LoadLockPositions(command, arguments);
                    //Load EnemyContainerPosition
                    //Load PlayerContainerPosition
                }
            }
        }

        public void SaveSettings()
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter("Settings.ini"))
            {
                //Flush to completely overwrite existing file instead of appending
                file.Flush();

                if (NetworkDeviceIndex != null)
                {
                    file.WriteLine("Device: " + NetworkDeviceIndex);
                }

                if (FriendlyPlayerContainerPosition != null)
                {

                    file.WriteLine("FriendlyContainerPos: " + FriendlyPlayerContainerPosition.Value.X + " " + FriendlyPlayerContainerPosition.Value.Y);
                }

                if (EnemyPlayerContainerPosition != null)
                {
                    file.WriteLine("EnemyContainerPos: " + EnemyPlayerContainerPosition.Value.X + " " + EnemyPlayerContainerPosition.Value.Y);
                }

                file.WriteLine("LockPositions: " + LockPositions);

                file.Close();
            }
        }
    }
}
