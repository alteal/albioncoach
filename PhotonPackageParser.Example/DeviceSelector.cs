﻿using PcapDotNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PhotonPackageParser.Program
{
    internal static class DeviceSelector
    {
        public static IList<LivePacketDevice> AllDevices;

        public static void DeterminePacketDevices()
        {
            AllDevices = LivePacketDevice.AllLocalMachine;

            if (AllDevices.Count == 0)
            {
                throw new Exception("No interfaces found! Make sure WinPcap is installed.");
            }

            // Print the list
            for (int i = 0; i != AllDevices.Count; ++i)
            {
                LivePacketDevice device = AllDevices[i];
                Console.Write((i + 1) + ". ");
                Console.Write(" " + string.Join(",",
                                  device.Addresses.Where(x => x.Address.Family == SocketAddressFamily.Internet)
                                      .Select(x => x.Address.ToString()).ToArray()) + " ");
                if (device.Description != null)
                    Console.WriteLine(" (" + device.Description + ")");
                else
                    Console.WriteLine(" (No description available)");
            }
        }

        public static PacketDevice AskForPacketDevice()
        {
            // Retrieve the device list from the local machine

            DeterminePacketDevices();

            int deviceIndex;
            do
            {

                string deviceIndexString;

                if (AllDevices.Count == 1) // If there is only 1 device,  take that one and continue
                {
                    deviceIndexString = 1.ToString(); // Dont make user fill in the interface when there is only one
                }
                else
                {
                    Console.WriteLine("Enter the interface number (1-" + AllDevices.Count + "):");
                     //deviceIndexString = Console.ReadLine();
                    deviceIndexString = 9.ToString();
                }
                
                if (!int.TryParse(deviceIndexString, out deviceIndex) ||
                    deviceIndex < 1 || deviceIndex > AllDevices.Count)
                {
                    deviceIndex = 0;
                }
            } while (deviceIndex == 0);

            return AllDevices[deviceIndex - 1];
        }
    }
}
