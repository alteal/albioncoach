﻿namespace PhotonPackageParser.Program
{
  public  partial class UserInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConsoleTbx = new MetroFramework.Controls.MetroTextBox();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // ConsoleTbx
            // 
            // 
            // 
            // 
            this.ConsoleTbx.CustomButton.Image = null;
            this.ConsoleTbx.CustomButton.Location = new System.Drawing.Point(148, 2);
            this.ConsoleTbx.CustomButton.Name = "";
            this.ConsoleTbx.CustomButton.Size = new System.Drawing.Size(533, 533);
            this.ConsoleTbx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.ConsoleTbx.CustomButton.TabIndex = 1;
            this.ConsoleTbx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.ConsoleTbx.CustomButton.UseSelectable = true;
            this.ConsoleTbx.CustomButton.Visible = false;
            this.ConsoleTbx.Lines = new string[0];
            this.ConsoleTbx.Location = new System.Drawing.Point(23, 108);
            this.ConsoleTbx.MaxLength = 32767;
            this.ConsoleTbx.Multiline = true;
            this.ConsoleTbx.Name = "ConsoleTbx";
            this.ConsoleTbx.PasswordChar = '\0';
            this.ConsoleTbx.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ConsoleTbx.SelectedText = "";
            this.ConsoleTbx.SelectionLength = 0;
            this.ConsoleTbx.SelectionStart = 0;
            this.ConsoleTbx.ShortcutsEnabled = true;
            this.ConsoleTbx.Size = new System.Drawing.Size(684, 538);
            this.ConsoleTbx.TabIndex = 5;
            this.ConsoleTbx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ConsoleTbx.UseSelectable = true;
            this.ConsoleTbx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.ConsoleTbx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.CommandsBorderColor = System.Drawing.Color.White;
            this.propertyGrid1.HelpBorderColor = System.Drawing.Color.Black;
            this.propertyGrid1.Location = new System.Drawing.Point(1397, 108);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(43, 538);
            this.propertyGrid1.TabIndex = 6;
            this.propertyGrid1.ViewBorderColor = System.Drawing.SystemColors.AppWorkspace;
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.Location = new System.Drawing.Point(735, 108);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(305, 223);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroButton1.TabIndex = 7;
            this.metroButton1.Text = "Stop Scanning";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(1093, 133);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(146, 112);
            this.metroButton2.TabIndex = 8;
            this.metroButton2.Text = "metroButton2";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // UserInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1463, 669);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.ConsoleTbx);
            this.Name = "UserInterface";
            this.Text = "UserInterface";
            this.Load += new System.EventHandler(this.UserInterface_Load);
            this.ResumeLayout(false);

        }

        #endregion
        public MetroFramework.Controls.MetroTextBox ConsoleTbx;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton2;
    }
}