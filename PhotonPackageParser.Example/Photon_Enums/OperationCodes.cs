﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonPackageParser.Program
{
  public  class OperationCodes
    {
/*
        public static List<string> OperationIgnore = new List<string>(new string[] {
        "CreateAccount", 
        "ExtendedHardwareStats"
        });
*/
        public static List<string> OperationAccept = new List<string>(new string[] {
         "HealthUpdate",
         "Join",
         "Login",
         "TakeSilver",
         "QueryGuildPlayerStats",
          "TrackAchievements",


           "Unused", // Indezx is row number - where this line starts.
                "Ping",
                "Join",
                "CreateAccount",
                "Login",
                "SendCrashLog",
                "CreateCharacter",
                "DeleteCharacter",
                "SelectCharacter",
                "RedeemKeycode",
                "GetGameServerByCluster",
                "GetSubscriptionDetails",
                "GetActiveSubscription",
                "GetSubscriptionUrl",
                "GetPurchaseGoldUrl",
                "GetBuyTrialDetails",
                "GetReferralSeasonDetails",
                "GetAvailableTrialKeys",
                //"Move", // player movement
                "AttackStart",
                "CastStart",
                "CastCancel",
                "TerminateToggleSpell",
                "ChannelingCancel",
                "AttackBuildingStart",
                "InventoryDestroyItem",
                "InventoryMoveItem",
                "InventorySplitStack",
                "ChangeCluster",
                "ConsoleCommand",
                "ChatMessage", // new Chatmessage
                "ReportClientError",
                "RegisterToObject",
                "UnRegisterFromObject",
                "CraftBuildingChangeSettings",
                "CraftBuildingTakeMoney",
                "RepairBuildingChangeSettings",
                "RepairBuildingTakeMoney",
                "ActionBuildingChangeSettings",
                "HarvestStart", // Start harvesting
                "HarvestCancel", // Stop harvesting
                "TakeSilver", // Pickup Silver?
                "ActionOnBuildingStart",
                "ActionOnBuildingCancel",
                "ItemRerollQualityStart",
                "ItemRerollQualityCancel",
                "InstallResourceStart",
                "InstallResourceCancel",
                "InstallSilver",
                "BuildingFillNutrition",
                "BuildingChangeRenovationState",
                "BuildingBuySkin",
                "BuildingClaim",
                "BuildingGiveup",
                "BuildingNutritionSilverStorageDeposit",
                "BuildingNutritionSilverStorageWithdraw",
                "BuildingNutritionSilverRewardSet",
                "ConstructionSiteCreate",
                "PlaceableItemPlace",
                "PlaceableItemPlaceCancel",
                "PlaceableObjectPickup",
                "FurnitureObjectUse",
                "FarmableHarvest",
                "FarmableFinishGrownItem",
                "FarmableDestroy",
                "FarmableGetProduct",
                "FarmableFill",
                "LaborerObjectPlace",
                "LaborerObjectPlaceCancel",
                "CastleGateUse",
                "AuctionCreateOffer",
                "AuctionCreateRequest",
                "AuctionGetOffers", // Get Sell orders Contains JSON STRING when it comes back as a RESPONSE
                "AuctionGetRequests", // Get buy orders Contains JSON STRING when it comes back as a RESPONSE
                "AuctionBuyOffer",
                "AuctionAbortAuction",
                "AuctionModifyAuction",
                "AuctionAbortOffer",
                "AuctionAbortRequest",
                "AuctionSellRequest",
                "AuctionGetFinishedAuctions",
                "AuctionFetchAuction",
                "AuctionGetMyOpenOffers",
                "AuctionGetMyOpenRequests",
                "AuctionGetMyOpenAuctions",
                "AuctionGetItemsAverage",
                "AuctionGetItemAverageStats",
                "AuctionGetItemAverageValue",
                "ContainerOpen",
                "ContainerClose",
                "ContainerManageSubContainer",
                "Respawn",
                "Suicide",
                "JoinGuild",
                "LeaveGuild",
                "CreateGuild",
                "InviteToGuild",
                "DeclineGuildInvitation",
                "KickFromGuild",
                "DuellingChallengePlayer",
                "DuellingAcceptChallenge",
                "DuellingDenyChallenge",
                "ChangeClusterTax",
                "ClaimTerritory",
                "GiveUpTerritory",
                "ChangeTerritoryAccessRights",
                "GetMonolithInfo",
                "GetClaimInfo",
                "GetAttackInfo",
                "GetTerritorySeasonPoints",
                "GetAttackSchedule",
                "ScheduleAttack",
                "GetMatches",
                "GetMatchDetails",
                "JoinMatch",
                "LeaveMatch",
                "ChangeChatSettings",
                "LogoutStart",
                "LogoutCancel",
                "ClaimOrbStart",
                "ClaimOrbCancel",
                "DepositToGuildAccount",
                "WithdrawalFromAccount",
                "ChangeGuildPayUpkeepFlag",
                "ChangeGuildTax",
                "GetMyTerritories",
                "MorganaCommand",
                "GetServerInfo",
                "InviteMercenaryToMatch",
                "SubscribeToCluster",
                "AnswerMercenaryInvitation",
                "GetCharacterEquipment",
                "GetCharacterSteamAchievements",
                "GetCharacterStats",
                "GetKillHistoryDetails",
                "LearnMasteryLevel",
                "ReSpecAchievement",
                "ChangeAvatar",
                "GetRankings",
                "GetRank",
                "GetGvgSeasonRankings",
                "GetGvgSeasonRank",
                "GetGvgSeasonHistoryRankings",
                "KickFromGvGMatch",
                "GetChestLogs",
                "GetAccessRightLogs",
                "InviteToPlayerTrade",
                "PlayerTradeCancel",
                "PlayerTradeInvitationAccept",
                "PlayerTradeAddItem",
                "PlayerTradeRemoveItem",
                "PlayerTradeAcceptTrade",
                "PlayerTradeSetSilverOrGold",
                "SendMiniMapPing",
                "Stuck",
                "BuyRealEstate",
                "ClaimRealEstate",
                "GiveUpRealEstate",
                "ChangeRealEstateOutline",
                "GetMailInfos",
                "ReadMail",
                "SendNewMail",
                "DeleteMail",
                "ClaimAttachmentFromMail",
                "UpdateLfgInfo",
                "GetLfgInfos",
                "GetMyGuildLfgInfo",
                "GetLfgDescriptionText",
                "LfgApplyToGuild",
                "AnswerLfgGuildApplication",
                "GetClusterInfo",
                "RegisterChatPeer",
                "SendChatMessage",
                "JoinChatChannel",
                "LeaveChatChannel",
                "SendWhisperMessage",
                "Say",
                "PlayEmote",
                "StopEmote",
                "GetClusterMapInfo",
                "AccessRightsChangeSettings",
                "Mount",
                "MountCancel",
                "BuyJourney",
                "SetSaleStatusForEstate",
                "ResolveGuildOrPlayerName",
                "GetRespawnInfos",
                "MakeHome",
                "LeaveHome",
                "ResurrectionReply",
                "AllianceCreate",
                "AllianceDisband",
                "AllianceGetMemberInfos",
                "AllianceInvite",
                "AllianceAnswerInvitation",
                "AllianceCancelInvitation",
                "AllianceKickGuild",
                "AllianceLeave",
                "AllianceChangeGoldPaymentFlag",
                "AllianceGetDetailInfo",
                "GetIslandInfos",
                "AbandonMyIsland",
                "BuyMyIsland",
                "BuyGuildIsland",
                "AbandonGuildIsland",
                "UpgradeMyIsland",
                "UpgradeGuildIsland",
                "TerritoryFillNutrition",
                "TeleportBack",
                "PartyInvitePlayer",
                "PartyAnswerInvitation",
                "PartyLeave",
                "PartyKickPlayer",
                "PartyMakeLeader",
                "PartyChangeLootSetting",
                "GetGuildMOTD",
                "SetGuildMOTD",
                "ExitEnterStart",
                "ExitEnterCancel",
                "AgentRequest",
                "GoldMarketGetBuyOffer",
                "GoldMarketGetBuyOfferFromSilver",
                "GoldMarketGetSellOffer",
                "GoldMarketGetSellOfferFromSilver",
                "GoldMarketBuyGold",
                "GoldMarketSellGold",
                "GoldMarketCreateSellOrder",
                "GoldMarketCreateBuyOrder",
                "GoldMarketGetInfos",
                "GoldMarketCancelOrder",
                "GoldMarketGetAverageInfo",
                "SiegeCampClaimStart",
                "SiegeCampClaimCancel",
                "TreasureChestUsingStart",
                "TreasureChestUsingCancel",
                "LaborerStartJob",
                "LaborerTakeJobLoot",
                "LaborerDismiss",
                "LaborerMove",
                "LaborerBuyItem",
                "LaborerUpgrade",
                "BuyPremium",
                "BuyTrial",
                "RealEstateGetAuctionData",
                "RealEstateBidOnAuction",
                "GetSiegeCampCooldown",
                "FriendInvite",
                "FriendAnswerInvitation",
                "FriendCancelnvitation",
                "FriendRemove",
                "InventoryStack",
                "InventorySort",
                "EquipmentItemChangeSpell",
                "ExpeditionRegister",
                "ExpeditionRegisterCancel",
                "JoinExpedition",
                "DeclineExpeditionInvitation",
                "VoteStart",
                "VoteDoVote",
                "RatingDoRate",
                "EnteringExpeditionStart",
                "EnteringExpeditionCancel",
                "ActivateExpeditionCheckPoint",
                "ArenaRegister",
                "ArenaRegisterCancel",
                "ArenaLeave",
                "JoinArenaMatch",
                "DeclineArenaInvitation",
                "EnteringArenaStart",
                "EnteringArenaCancel",
                "ArenaCustomMatch",
                "UpdateCharacterStatement",
                "BoostFarmable",
                "GetStrikeHistory",
                "UseFunction",
                "UsePortalEntrance",
                "QueryPortalBinding",
                "ClaimPaymentTransaction",
                "ChangeUseFlag",
                "ClientPerformanceStats",
                "ExtendedHardwareStats",
                "TerritoryClaimStart",
                "TerritoryClaimCancel",
                "RequestAppStoreProducts",
                "VerifyProductPurchase",
                "QueryGuildPlayerStats",
                "TrackAchievements",
                "DepositItemToGuildCurrency",
                "WithdrawalItemFromGuildCurrency",
                "AuctionSellSpecificItemRequest",
                "FishingStart",
                "FishingCasting",
                "FishingCast",
                "FishingCatch",
                "FishingPull",
                "FishingGiveLine",
                "FishingFinish",
                "FishingCancel",
                "CreateGuildAccessTag",
                "DeleteGuildAccessTag",
                "RenameGuildAccessTag",
                "FlagGuildAccessTagGuildPermission",
                "AssignGuildAccessTag",
                "RemoveGuildAccessTagFromPlayer",
                "ModifyGuildAccessTagEditors",
                "RequestPublicAccessTags",
                "ChangeAccessTagPublicFlag",
                "UpdateGuildAccessTag",
                "SteamStartMicrotransaction",
                "SteamFinishMicrotransaction",
                "SteamIdHasActiveAccount",
                "CheckEmailAccountState",
                "LinkAccountToSteamId",
                "BuyGvgSeasonBooster",
                "ChangeFlaggingPrepare",
                "OverCharge",
                "OverChargeEnd",
                "RequestTrusted",
                "GetPersonalSeasonTrackerData",
                "UseConsumableFromInventory",
                "ClaimPersonalSeasonReward"
        });


        public static  List<string> OperationToString = new List<string>(new string[] {
                "Unused", // Indezx is row number - where this line starts.
                "Ping",
                "Join",
                "CreateAccount",
                "Login",
                "SendCrashLog",
                "CreateCharacter",
                "DeleteCharacter",
                "SelectCharacter",
                "RedeemKeycode",
                "GetGameServerByCluster",
                "GetSubscriptionDetails",
                "GetActiveSubscription",
                "GetSubscriptionUrl",
                "GetPurchaseGoldUrl",
                "GetBuyTrialDetails",
                "GetReferralSeasonDetails",
                "GetAvailableTrialKeys",
                "Move", // player movement
                "AttackStart",
                "CastStart",
                "CastCancel",
                "TerminateToggleSpell",
                "ChannelingCancel",
                "AttackBuildingStart",
                "InventoryDestroyItem",
                "InventoryMoveItem",
                "InventorySplitStack",
                "ChangeCluster",
                "ConsoleCommand",
                "ChatMessage", // new Chatmessage
                "ReportClientError",
                "RegisterToObject",
                "UnRegisterFromObject",
                "CraftBuildingChangeSettings",
                "CraftBuildingTakeMoney",
                "RepairBuildingChangeSettings",
                "RepairBuildingTakeMoney",
                "ActionBuildingChangeSettings",
                "HarvestStart", // Start harvesting
                "HarvestCancel", // Stop harvesting
                "TakeSilver", // Pickup Silver?
                "ActionOnBuildingStart",
                "ActionOnBuildingCancel",
                "ItemRerollQualityStart",
                "ItemRerollQualityCancel",
                "InstallResourceStart",
                "InstallResourceCancel",
                "InstallSilver",
                "BuildingFillNutrition",
                "BuildingChangeRenovationState",
                "BuildingBuySkin",
                "BuildingClaim",
                "BuildingGiveup",
                "BuildingNutritionSilverStorageDeposit",
                "BuildingNutritionSilverStorageWithdraw",
                "BuildingNutritionSilverRewardSet",
                "ConstructionSiteCreate",
                "PlaceableItemPlace",
                "PlaceableItemPlaceCancel",
                "PlaceableObjectPickup",
                "FurnitureObjectUse",
                "FarmableHarvest",
                "FarmableFinishGrownItem",
                "FarmableDestroy",
                "FarmableGetProduct",
                "FarmableFill",
                "LaborerObjectPlace",
                "LaborerObjectPlaceCancel",
                "CastleGateUse",
                "AuctionCreateOffer",
                "AuctionCreateRequest",
                "AuctionGetOffers", // Get Sell orders Contains JSON STRING when it comes back as a RESPONSE
                "AuctionGetRequests", // Get buy orders Contains JSON STRING when it comes back as a RESPONSE
                "AuctionBuyOffer",
                "AuctionAbortAuction",
                "AuctionModifyAuction",
                "AuctionAbortOffer",
                "AuctionAbortRequest",
                "AuctionSellRequest",
                "AuctionGetFinishedAuctions",
                "AuctionFetchAuction",
                "AuctionGetMyOpenOffers",
                "AuctionGetMyOpenRequests",
                "AuctionGetMyOpenAuctions",
                "AuctionGetItemsAverage",
                "AuctionGetItemAverageStats",
                "AuctionGetItemAverageValue",
                "ContainerOpen",
                "ContainerClose",
                "ContainerManageSubContainer",
                "Respawn",
                "Suicide",
                "JoinGuild",
                "LeaveGuild",
                "CreateGuild",
                "InviteToGuild",
                "DeclineGuildInvitation",
                "KickFromGuild",
                "DuellingChallengePlayer",
                "DuellingAcceptChallenge",
                "DuellingDenyChallenge",
                "ChangeClusterTax",
                "ClaimTerritory",
                "GiveUpTerritory",
                "ChangeTerritoryAccessRights",
                "GetMonolithInfo",
                "GetClaimInfo",
                "GetAttackInfo",
                "GetTerritorySeasonPoints",
                "GetAttackSchedule",
                "ScheduleAttack",
                "GetMatches",
                "GetMatchDetails",
                "JoinMatch",
                "LeaveMatch",
                "ChangeChatSettings",
                "LogoutStart",
                "LogoutCancel",
                "ClaimOrbStart",
                "ClaimOrbCancel",
                "DepositToGuildAccount",
                "WithdrawalFromAccount",
                "ChangeGuildPayUpkeepFlag",
                "ChangeGuildTax",
                "GetMyTerritories",
                "MorganaCommand",
                "GetServerInfo",
                "InviteMercenaryToMatch",
                "SubscribeToCluster",
                "AnswerMercenaryInvitation",
                "GetCharacterEquipment",
                "GetCharacterSteamAchievements",
                "GetCharacterStats",
                "GetKillHistoryDetails",
                "LearnMasteryLevel",
                "ReSpecAchievement",
                "ChangeAvatar",
                "GetRankings",
                "GetRank",
                "GetGvgSeasonRankings",
                "GetGvgSeasonRank",
                "GetGvgSeasonHistoryRankings",
                "KickFromGvGMatch",
                "GetChestLogs",
                "GetAccessRightLogs",
                "InviteToPlayerTrade",
                "PlayerTradeCancel",
                "PlayerTradeInvitationAccept",
                "PlayerTradeAddItem",
                "PlayerTradeRemoveItem",
                "PlayerTradeAcceptTrade",
                "PlayerTradeSetSilverOrGold",
                "SendMiniMapPing",
                "Stuck",
                "BuyRealEstate",
                "ClaimRealEstate",
                "GiveUpRealEstate",
                "ChangeRealEstateOutline",
                "GetMailInfos",
                "ReadMail",
                "SendNewMail",
                "DeleteMail",
                "ClaimAttachmentFromMail",
                "UpdateLfgInfo",
                "GetLfgInfos",
                "GetMyGuildLfgInfo",
                "GetLfgDescriptionText",
                "LfgApplyToGuild",
                "AnswerLfgGuildApplication",
                "GetClusterInfo",
                "RegisterChatPeer",
                "SendChatMessage",
                "JoinChatChannel",
                "LeaveChatChannel",
                "SendWhisperMessage",
                "Say",
                "PlayEmote",
                "StopEmote",
                "GetClusterMapInfo",
                "AccessRightsChangeSettings",
                "Mount",
                "MountCancel",
                "BuyJourney",
                "SetSaleStatusForEstate",
                "ResolveGuildOrPlayerName",
                "GetRespawnInfos",
                "MakeHome",
                "LeaveHome",
                "ResurrectionReply",
                "AllianceCreate",
                "AllianceDisband",
                "AllianceGetMemberInfos",
                "AllianceInvite",
                "AllianceAnswerInvitation",
                "AllianceCancelInvitation",
                "AllianceKickGuild",
                "AllianceLeave",
                "AllianceChangeGoldPaymentFlag",
                "AllianceGetDetailInfo",
                "GetIslandInfos",
                "AbandonMyIsland",
                "BuyMyIsland",
                "BuyGuildIsland",
                "AbandonGuildIsland",
                "UpgradeMyIsland",
                "UpgradeGuildIsland",
                "TerritoryFillNutrition",
                "TeleportBack",
                "PartyInvitePlayer",
                "PartyAnswerInvitation",
                "PartyLeave",
                "PartyKickPlayer",
                "PartyMakeLeader",
                "PartyChangeLootSetting",
                "GetGuildMOTD",
                "SetGuildMOTD",
                "ExitEnterStart",
                "ExitEnterCancel",
                "AgentRequest",
                "GoldMarketGetBuyOffer",
                "GoldMarketGetBuyOfferFromSilver",
                "GoldMarketGetSellOffer",
                "GoldMarketGetSellOfferFromSilver",
                "GoldMarketBuyGold",
                "GoldMarketSellGold",
                "GoldMarketCreateSellOrder",
                "GoldMarketCreateBuyOrder",
                "GoldMarketGetInfos",
                "GoldMarketCancelOrder",
                "GoldMarketGetAverageInfo",
                "SiegeCampClaimStart",
                "SiegeCampClaimCancel",
                "TreasureChestUsingStart",
                "TreasureChestUsingCancel",
                "LaborerStartJob",
                "LaborerTakeJobLoot",
                "LaborerDismiss",
                "LaborerMove",
                "LaborerBuyItem",
                "LaborerUpgrade",
                "BuyPremium",
                "BuyTrial",
                "RealEstateGetAuctionData",
                "RealEstateBidOnAuction",
                "GetSiegeCampCooldown",
                "FriendInvite",
                "FriendAnswerInvitation",
                "FriendCancelnvitation",
                "FriendRemove",
                "InventoryStack",
                "InventorySort",
                "EquipmentItemChangeSpell",
                "ExpeditionRegister",
                "ExpeditionRegisterCancel",
                "JoinExpedition",
                "DeclineExpeditionInvitation",
                "VoteStart",
                "VoteDoVote",
                "RatingDoRate",
                "EnteringExpeditionStart",
                "EnteringExpeditionCancel",
                "ActivateExpeditionCheckPoint",
                "ArenaRegister",
                "ArenaRegisterCancel",
                "ArenaLeave",
                "JoinArenaMatch",
                "DeclineArenaInvitation",
                "EnteringArenaStart",
                "EnteringArenaCancel",
                "ArenaCustomMatch",
                "UpdateCharacterStatement",
                "BoostFarmable",
                "GetStrikeHistory",
                "UseFunction",
                "UsePortalEntrance",
                "QueryPortalBinding",
                "ClaimPaymentTransaction",
                "ChangeUseFlag",
                "ClientPerformanceStats",
                "ExtendedHardwareStats",
                "TerritoryClaimStart",
                "TerritoryClaimCancel",
                "RequestAppStoreProducts",
                "VerifyProductPurchase",
                "QueryGuildPlayerStats",
                "TrackAchievements",
                "DepositItemToGuildCurrency",
                "WithdrawalItemFromGuildCurrency",
                "AuctionSellSpecificItemRequest",
                "FishingStart",
                "FishingCasting",
                "FishingCast",
                "FishingCatch",
                "FishingPull",
                "FishingGiveLine",
                "FishingFinish",
                "FishingCancel",
                "CreateGuildAccessTag",
                "DeleteGuildAccessTag",
                "RenameGuildAccessTag",
                "FlagGuildAccessTagGuildPermission",
                "AssignGuildAccessTag",
                "RemoveGuildAccessTagFromPlayer",
                "ModifyGuildAccessTagEditors",
                "RequestPublicAccessTags",
                "ChangeAccessTagPublicFlag",
                "UpdateGuildAccessTag",
                "SteamStartMicrotransaction",
                "SteamFinishMicrotransaction",
                "SteamIdHasActiveAccount",
                "CheckEmailAccountState",
                "LinkAccountToSteamId",
                "BuyGvgSeasonBooster",
                "ChangeFlaggingPrepare",
                "OverCharge",
                "OverChargeEnd",
                "RequestTrusted",
                "GetPersonalSeasonTrackerData",
                "UseConsumableFromInventory",
                "ClaimPersonalSeasonReward"
            }
                    );


        public enum OperationCodes_Enum
        {
            Unused = 0,
            Ping = 1,
            Join = 2,
            CreateAccount = 3,
            Login = 4,
            SendCrashLog = 5,
            CreateCharacter = 6,
            DeleteCharacter = 7,
            SelectCharacter = 8,
            RedeemKeycode = 9,
            GetGameServerByCluster = 10,
            GetSubscriptionDetails = 11,
            GetActiveSubscription = 12,
            GetSubscriptionUrl = 13,
            GetPurchaseGoldUrl = 14,
            GetBuyTrialDetails = 15,
            GetReferralSeasonDetails = 16,
            GetAvailableTrialKeys = 17,
            Move = 18, // player movement
            AttackStart = 19,
            CastStart = 20,
            CastCancel = 21,
            TerminateToggleSpell = 22,
            ChannelingCancel = 23,
            AttackBuildingStart = 24,
            InventoryDestroyItem = 25,
            InventoryMoveItem = 26,
            InventorySplitStack = 27,
            ChangeCluster = 28,
            ConsoleCommand = 29,
            ChatMessage = 30, // new Chatmessage
            ReportClientError = 31,
            RegisterToObject = 32,
            UnRegisterFromObject = 33,
            CraftBuildingChangeSettings = 34,
            CraftBuildingTakeMoney = 35,
            RepairBuildingChangeSettings = 36,
            RepairBuildingTakeMoney = 37,
            ActionBuildingChangeSettings = 38,
            HarvestStart = 39, // Start harvesting
            HarvestCancel = 40, // Stop harvesting
            TakeSilver = 41, // Pickup Silver?
            ActionOnBuildingStart = 42,
            ActionOnBuildingCancel = 43,
            ItemRerollQualityStart = 44,
            ItemRerollQualityCancel = 45,
            InstallResourceStart = 46,
            InstallResourceCancel = 47,
            InstallSilver = 48,
            BuildingFillNutrition = 49,
            BuildingChangeRenovationState = 50,
            BuildingBuySkin = 51,
            BuildingClaim = 52,
            BuildingGiveup = 53,
            BuildingNutritionSilverStorageDeposit = 54,
            BuildingNutritionSilverStorageWithdraw = 55,
            BuildingNutritionSilverRewardSet = 56,
            ConstructionSiteCreate = 57,
            PlaceableItemPlace = 58,
            PlaceableItemPlaceCancel = 59,
            PlaceableObjectPickup = 60,
            FurnitureObjectUse = 61,
            FarmableHarvest = 62,
            FarmableFinishGrownItem = 63,
            FarmableDestroy = 64,
            FarmableGetProduct = 65,
            FarmableFill = 66,
            LaborerObjectPlace = 67,
            LaborerObjectPlaceCancel = 68,
            CastleGateUse = 69,
            AuctionCreateOffer = 70,
            AuctionCreateRequest = 71,
            AuctionGetOffers = 72, // Get Sell orders
            AuctionGetRequests = 73, // Get buy orders
            AuctionBuyOffer = 74,
            AuctionAbortAuction = 75,
            AuctionModifyAuction = 76,
            AuctionAbortOffer = 77,
            AuctionAbortRequest = 78,
            AuctionSellRequest = 79,
            AuctionGetFinishedAuctions = 80,
            AuctionFetchAuction = 81,
            AuctionGetMyOpenOffers = 82,
            AuctionGetMyOpenRequests = 83,
            AuctionGetMyOpenAuctions = 84,
            AuctionGetItemsAverage = 85,
            AuctionGetItemAverageStats = 86,
            AuctionGetItemAverageValue = 87,
            ContainerOpen = 88,
            ContainerClose = 89,
            ContainerManageSubContainer = 90,
            Respawn = 91,
            Suicide = 92,
            JoinGuild = 93,
            LeaveGuild = 94,
            CreateGuild = 95,
            InviteToGuild = 96,
            DeclineGuildInvitation = 97,
            KickFromGuild = 98,
            DuellingChallengePlayer = 99,
            DuellingAcceptChallenge = 100,
            DuellingDenyChallenge = 101,
            ChangeClusterTax = 102,
            ClaimTerritory = 103,
            GiveUpTerritory = 104,
            ChangeTerritoryAccessRights = 105,
            GetMonolithInfo = 106,
            GetClaimInfo = 107,
            GetAttackInfo = 108,
            GetTerritorySeasonPoints = 109,
            GetAttackSchedule = 110,
            ScheduleAttack = 111,
            GetMatches = 112,
            GetMatchDetails = 113,
            JoinMatch = 114,
            LeaveMatch = 115,
            ChangeChatSettings = 116,
            LogoutStart = 117,
            LogoutCancel = 118,
            ClaimOrbStart = 119,
            ClaimOrbCancel = 120,
            DepositToGuildAccount = 121,
            WithdrawalFromAccount = 122,
            ChangeGuildPayUpkeepFlag = 123,
            ChangeGuildTax = 124,
            GetMyTerritories = 125,
            MorganaCommand = 126,
            GetServerInfo = 127,
            InviteMercenaryToMatch = 128,
            SubscribeToCluster = 129,
            AnswerMercenaryInvitation = 130,
            GetCharacterEquipment = 131,
            GetCharacterSteamAchievements = 132,
            GetCharacterStats = 133,
            GetKillHistoryDetails = 134,
            LearnMasteryLevel = 135,
            ReSpecAchievement = 136,
            ChangeAvatar = 137,
            GetRankings = 138,
            GetRank = 139,
            GetGvgSeasonRankings = 140,
            GetGvgSeasonRank = 141,
            GetGvgSeasonHistoryRankings = 142,
            KickFromGvGMatch = 143,
            GetChestLogs = 144,
            GetAccessRightLogs = 145,
            InviteToPlayerTrade = 146,
            PlayerTradeCancel = 147,
            PlayerTradeInvitationAccept = 148,
            PlayerTradeAddItem = 149,
            PlayerTradeRemoveItem = 150,
            PlayerTradeAcceptTrade = 151,
            PlayerTradeSetSilverOrGold = 152,
            SendMiniMapPing = 153,
            Stuck = 154,
            BuyRealEstate = 155,
            ClaimRealEstate = 156,
            GiveUpRealEstate = 157,
            ChangeRealEstateOutline = 158,
            GetMailInfos = 159,
            ReadMail = 160,
            SendNewMail = 161,
            DeleteMail = 162,
            ClaimAttachmentFromMail = 163,
            UpdateLfgInfo = 164,
            GetLfgInfos = 165,
            GetMyGuildLfgInfo = 166,
            GetLfgDescriptionText = 167,
            LfgApplyToGuild = 168,
            AnswerLfgGuildApplication = 169,
            GetClusterInfo = 170,
            RegisterChatPeer = 171,
            SendChatMessage = 172,
            JoinChatChannel = 173,
            LeaveChatChannel = 174,
            SendWhisperMessage = 175,
            Say = 176,
            PlayEmote = 177,
            StopEmote = 178,
            GetClusterMapInfo = 179,
            AccessRightsChangeSettings = 180,
            Mount = 181,
            MountCancel = 182,
            BuyJourney = 183,
            SetSaleStatusForEstate = 184,
            ResolveGuildOrPlayerName = 185,
            GetRespawnInfos = 186,
            MakeHome = 187,
            LeaveHome = 188,
            ResurrectionReply = 189,
            AllianceCreate = 190,
            AllianceDisband = 191,
            AllianceGetMemberInfos = 192,
            AllianceInvite = 193,
            AllianceAnswerInvitation = 194,
            AllianceCancelInvitation = 195,
            AllianceKickGuild = 196,
            AllianceLeave = 197,
            AllianceChangeGoldPaymentFlag = 198,
            AllianceGetDetailInfo = 199,
            GetIslandInfos = 200,
            AbandonMyIsland = 201,
            BuyMyIsland = 202,
            BuyGuildIsland = 203,
            AbandonGuildIsland = 204,
            UpgradeMyIsland = 205,
            UpgradeGuildIsland = 206,
            TerritoryFillNutrition = 207,
            TeleportBack = 208,
            PartyInvitePlayer = 209,
            PartyAnswerInvitation = 210,
            PartyLeave = 211,
            PartyKickPlayer = 212,
            PartyMakeLeader = 213,
            PartyChangeLootSetting = 214,
            GetGuildMOTD = 215,
            SetGuildMOTD = 216,
            ExitEnterStart = 217,
            ExitEnterCancel = 218,
            AgentRequest = 219,
            GoldMarketGetBuyOffer = 220,
            GoldMarketGetBuyOfferFromSilver = 221,
            GoldMarketGetSellOffer = 222,
            GoldMarketGetSellOfferFromSilver = 223,
            GoldMarketBuyGold = 224,
            GoldMarketSellGold = 225,
            GoldMarketCreateSellOrder = 226,
            GoldMarketCreateBuyOrder = 227,
            GoldMarketGetInfos = 228,
            GoldMarketCancelOrder = 229,
            GoldMarketGetAverageInfo = 230,
            SiegeCampClaimStart = 231,
            SiegeCampClaimCancel = 232,
            TreasureChestUsingStart = 233,
            TreasureChestUsingCancel = 234,
            LaborerStartJob = 235,
            LaborerTakeJobLoot = 236,
            LaborerDismiss = 237,
            LaborerMove = 238,
            LaborerBuyItem = 239,
            LaborerUpgrade = 240,
            BuyPremium = 241,
            BuyTrial = 242,
            RealEstateGetAuctionData = 243,
            RealEstateBidOnAuction = 244,
            GetSiegeCampCooldown = 245,
            FriendInvite = 246,
            FriendAnswerInvitation = 247,
            FriendCancelnvitation = 248,
            FriendRemove = 249,
            InventoryStack = 250,
            InventorySort = 251,
            EquipmentItemChangeSpell = 252,
            ExpeditionRegister = 253,
            ExpeditionRegisterCancel = 254,
            JoinExpedition = 255,
            DeclineExpeditionInvitation = 256,
            VoteStart = 257,
            VoteDoVote = 258,
            RatingDoRate = 259,
            EnteringExpeditionStart = 260,
            EnteringExpeditionCancel = 261,
            ActivateExpeditionCheckPoint = 262,
            ArenaRegister = 263,
            ArenaRegisterCancel = 264,
            ArenaLeave = 265,
            JoinArenaMatch = 266,
            DeclineArenaInvitation = 267,
            EnteringArenaStart = 268,
            EnteringArenaCancel = 269,
            ArenaCustomMatch = 270,
            UpdateCharacterStatement = 271,
            BoostFarmable = 272,
            GetStrikeHistory = 273,
            UseFunction = 274,
            UsePortalEntrance = 275,
            QueryPortalBinding = 276,
            ClaimPaymentTransaction = 277,
            ChangeUseFlag = 278,
            ClientPerformanceStats = 279,
            ExtendedHardwareStats = 280,
            TerritoryClaimStart = 281,
            TerritoryClaimCancel = 282,
            RequestAppStoreProducts = 283,
            VerifyProductPurchase = 284,
            QueryGuildPlayerStats = 285,
            TrackAchievements = 286,
            DepositItemToGuildCurrency = 287,
            WithdrawalItemFromGuildCurrency = 288,
            AuctionSellSpecificItemRequest = 289,
            FishingStart = 290,
            FishingCasting = 291,
            FishingCast = 292,
            FishingCatch = 293,
            FishingPull = 294,
            FishingGiveLine = 295,
            FishingFinish = 296,
            FishingCancel = 297,
            CreateGuildAccessTag = 298,
            DeleteGuildAccessTag = 299,
            RenameGuildAccessTag = 300,
            FlagGuildAccessTagGuildPermission = 301,
            AssignGuildAccessTag = 302,
            RemoveGuildAccessTagFromPlayer = 303,
            ModifyGuildAccessTagEditors = 304,
            RequestPublicAccessTags = 305,
            ChangeAccessTagPublicFlag = 306,
            UpdateGuildAccessTag = 307,
            SteamStartMicrotransaction = 308,
            SteamFinishMicrotransaction = 309,
            SteamIdHasActiveAccount = 310,
            CheckEmailAccountState = 311,
            LinkAccountToSteamId = 312,
            BuyGvgSeasonBooster = 313,
            ChangeFlaggingPrepare = 314,
            OverCharge = 315,
            OverChargeEnd = 316,
            RequestTrusted = 317,
            GetPersonalSeasonTrackerData = 318,
            UseConsumableFromInventory = 319,
            ClaimPersonalSeasonReward = 320
        }


    }

}
