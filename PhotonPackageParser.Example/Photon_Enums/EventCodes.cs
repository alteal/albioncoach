﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonPackageParser.Program
{
  public  class EventCodes
    {
/*
        public static List<string> EventIgnore = new List<string>(new string[] {
        "None",
        "Leave",
        "JoinFinished",
        "TimeSync",
        "PlayerCounts",
        "CastSpell",
        "CastHit",
        "ActiveSpellEffectsUpdate",
        "NewMob",
        "ChannelingEnded",
        "CastStart",
        "EnergyUpdate",
        "CastFinished",
        "NewSpellEffectArea",
        "ForcedMovement",
        "GuildUpdate",
        "NewCharacter",
        "Mounted"
        });
*/
        public static List<string> EventAccept = new List<string>(new string[] {
        "NewCharacter",
        "HealthUpdate",
        "CharacterStats",
        "TakeSilver",

        
        "JoinFinished",
        "HealthUpdate", // Damage   ObjectID,GameTimeStampHealthChange,NewHealthValue,EffectType,EffectOrigin,CauserId,CausingSpellType;
        "EnergyUpdate",
            //EnergyUpdate:
            //key0 = possible player ID
            //key1 = timestamp
            //key2 = mana cost
            //key3 = remaining mana
            //key4
            //key5 = possible player ID
            //key6 = possible skill type
        "Attack",
        "InventoryPutItem",
        "InventoryDeleteItem",
        "NewCharacter", // New character has entered the vieuw
        "NewSilverObject",
        "HarvestableChangeState",
        "MobChangeState",
        "TakeSilver", // Somebody takes silver.
        "ChatMessage",
        "ChatSay",
        "ChatWhisper",  
        "UpdateMoney", // Update your money
        "UpdateFame", // Update fame  
        "UpdateCurrency", // Update gold ?       
        "GuildUpdate",
        "GuildPlayerUpdated",
        "GuildMemberWorldUpdate",
        "NewMob",
        "ClaimOrbStart", // GVG ORBS?
        "ClaimOrbFinished",// GVG ORBS?
        "ClaimOrbCancel",// GVG ORBS?
        "OrbUpdate",// GVG ORBS?
        "OrbClaimed",// GVG ORBS?
        "NewWarCampObject",
        "GuildMemberTerritoryUpdate",
        "InvitedMercenaryToMatch",
        "ClusterInfoUpdate", // Cluster info,  MAYBE a name        
        "CharacterStats",
        "CharacterStatsKillHistory",
        "CharacterStatsDeathHistory",
        "GuildStats",
        "FullAchievementInfo",
        "FinishedAchievement",
        "AchievementProgressInfo",
        "FullAchievementProgressInfo",
        "FullTrackedAchievementInfo",
        "GameEvent",
        "MiniMapPing", //PING
        "MinimapPlayerPositions", // Party Player positions
        "MarketPlaceNotification", // Market mail ?        
        "NewUnreadMails",
        "GuildLogoObjectUpdate",        
        "NewChatChannels",
        "JoinedChatChannel",
        "LeftChatChannel",
        "RemovedChatChannel",        
        "Cloak", // Invis ?
        "PartyInvitation",
        "PartyJoined",
        "PartyDisbanded",
        "PartyPlayerJoined",
        "PartyChangedOrder",
        "PartyPlayerLeft",
        "PartyLeaderChanged",
        "PartyLootSettingChangedPlayer",
        "PartySilverGained",
        "PartyPlayerUpdated",
        "PartyInvitationPlayerBusy",        
        "OtherGrabbedLoot", // Loot log.        
        "FriendRequest",
        "FriendRequestInfos",
        "FriendInfos",
        "FriendRequestAnswered",
        "FriendOnlineStatus",
        "FriendRequestCanceled",
        "FriendRemoved",
        "FriendUpdated",
        "PartyLootItems",
        "PartyLootItemsRemoved",       
        "GuildFullAccessTagsUpdated",
        "GuildAccessTagUpdated",
        "GvgSeasonUpdate",
        "GvgSeasonCheatCommand",
        "SeasonPointsByKillingBooster",
        "OverChargeEnd",
        "OverChargeStatus",
        "OutpostReward",
        "NewUnlockedPersonalSeasonRewards" ,
        "PersonalSeasonPointsGained"
        });


        public static  List<string> EventToString = new List<string>(new string[] {
        "None", // Indezx is row number - where this line starts.
        "Leave",
        "JoinFinished",
        "Move",
        "Teleport",
        "ChangeEquipment",
        "HealthUpdate", // Damage   ObjectID,GameTimeStampHealthChange,NewHealthValue,EffectType,EffectOrigin,CauserId,CausingSpellType;
        "EnergyUpdate",
        "DamageShieldUpdate",
        "CraftingFocusUpdate",
        "ActiveSpellEffectsUpdate",
        "ResetCooldowns",
        "Attack",
        "CastStart",
        "CastCancel",
        "CastTimeUpdate",
        "CastFinished",
        "CastSpell",
        "CastHit",
        "ChannelingEnded",
        "AttackBuilding",
        "InventoryPutItem",
        "InventoryDeleteItem",
        "NewCharacter",
        "NewEquipmentItem",
        "NewSimpleItem",
        "NewFurnitureItem",
        "NewJournalItem",
        "NewSimpleHarvestableObject",
        "NewSimpleHarvestableObjectList",
        "NewHarvestableObject",
        "NewSilverObject",
        "NewBuilding",
        "HarvestableChangeState",
        "MobChangeState",
        "FactionBuildingInfo",
        "CraftBuildingInfo",
        "RepairBuildingInfo",
        "MeldBuildingInfo",
        "ConstructionSiteInfo",
        "PlayerBuildingInfo",
        "FarmBuildingInfo",
        "LaborerObjectInfo",
        "LaborerObjectJobInfo",
        "MarketPlaceBuildingInfo",
        "HarvestStart",
        "HarvestCancel",
        "HarvestFinished",
        "TakeSilver", // Silver taken from the ground
        "ActionOnBuildingStart",
        "ActionOnBuildingCancel",
        "ActionOnBuildingFinished",
        "ItemRerollQualityStart",
        "ItemRerollQualityCancel",
        "ItemRerollQualityFinished",
        "InstallResourceStart",
        "InstallResourceCancel",
        "InstallResourceFinished",
        "CraftItemFinished",
        "LogoutCancel",
        "ChatMessage",
        "ChatSay",
        "ChatWhisper",
        "PlayEmote",
        "StopEmote",
        "SystemMessage",
        "UpdateMoney",
        "UpdateFame", // Update fame
        "UpdateLearningPoints",
        "UpdateReSpecPoints",
        "UpdateCurrency",
        "UpdateFactionStanding",
        "Respawn",
        "ServerDebugLog",
        "CharacterEquipmentChanged",
        "RegenerationHealthChanged",
        "RegenerationEnergyChanged",
        "RegenerationMountHealthChanged",
        "RegenerationCraftingChanged",
        "RegenerationHealthEnergyComboChanged",
        "RegenerationPlayerComboChanged",
        "DurabilityChanged",
        "NewLoot",
        "ContainerInfo",
        "GuildVaultInfo",
        "GuildUpdate",
        "GuildPlayerUpdated",
        "InvitedToGuild",
        "GuildMemberWorldUpdate",
        "UpdateMatchDetails",
        "ObjectEvent",
        "NewMonolithObject",
        "NewSiegeCampObject",
        "NewOrbObject",
        "NewCastleObject",
        "NewSpellEffectArea",
        "NewChainSpell",
        "UpdateChainSpell",
        "NewTreasureChest",
        "StartMatch",
        "StartTerritoryMatchInfos",
        "StartArenaMatchInfos",
        "EndTerritoryMatch",
        "EndArenaMatch",
        "MatchUpdate",
        "ActiveMatchUpdate",
        "NewMob",
        "DebugAggroInfo",
        "DebugVariablesInfo",
        "DebugReputationInfo",
        "DebugDiminishingReturnInfo",
        "ClaimOrbStart",
        "ClaimOrbFinished",
        "ClaimOrbCancel",
        "OrbUpdate",
        "OrbClaimed",
        "NewWarCampObject",
        "GuildMemberTerritoryUpdate",
        "InvitedMercenaryToMatch",
        "ClusterInfoUpdate",
        "ForcedMovement",
        "ForcedMovementCancel",
        "CharacterStats",
        "CharacterStatsKillHistory",
        "CharacterStatsDeathHistory",
        "GuildStats",
        "KillHistoryDetails",
        "FullAchievementInfo",
        "FinishedAchievement",
        "AchievementProgressInfo",
        "FullAchievementProgressInfo",
        "FullTrackedAchievementInfo",
        "AgentQuestOffered",
        "AgentDebugInfo",
        "ConsoleEvent",
        "TimeSync",
        "ChangeAvatar",
        "GameEvent",
        "KilledPlayer",
        "Died",
        "KnockedDown",
        "MatchPlayerJoinedEvent",
        "MatchPlayerStatsEvent",
        "MatchPlayerStatsCompleteEvent",
        "MatchTimeLineEventEvent",
        "MatchPlayerMainGearStatsEvent",
        "MatchPlayerChangedAvatarEvent",
        "InvitationPlayerTrade",
        "PlayerTradeStart",
        "PlayerTradeCancel",
        "PlayerTradeUpdate",
        "PlayerTradeFinished",
        "PlayerTradeAcceptChange",
        "MiniMapPing",
        "MinimapPlayerPositions",
        "MarketPlaceNotification",
        "DuellingChallengePlayer",
        "NewDuellingPost",
        "DuelStarted",
        "DuelEnded",
        "DuelDenied",
        "DuelLeftArea",
        "DuelReEnteredArea",
        "NewRealEstate",
        "MiniMapOwnedBuildingsPositions",
        "RealEstateListUpdate",
        "GuildLogoUpdate",
        "PlaceableItemPlace",
        "PlaceableItemPlaceCancel",
        "FurnitureObjectBuffProviderInfo",
        "FurnitureObjectCheatProviderInfo",
        "FarmableObjectInfo",
        "LaborerObjectPlace",
        "LaborerObjectPlaceCancel",
        "NewUnreadMails",
        "GuildLogoObjectUpdate",
        "StartLogout",
        "NewChatChannels",
        "JoinedChatChannel",
        "LeftChatChannel",
        "RemovedChatChannel",
        "AccessStatus",
        "Mounted",
        "MountCancel",
        "NewTravelpoint",
        "NewIslandAccessPoint",
        "NewExit",
        "UpdateHome",
        "UpdateChatSettings",
        "ResurrectionOffer",
        "ResurrectionReply",
        "LootEquipmentChanged",
        "UpdateUnlockedGuildLogos",
        "UpdateUnlockedAvatars",
        "UpdateUnlockedAvatarRings",
        "UpdateUnlockedBuildings",
        "vDailyLoginBonus",
        "NewIslandManagement",
        "NewTeleportStone",
        "Cloak",
        "PartyInvitation",
        "PartyJoined",
        "PartyDisbanded",
        "PartyPlayerJoined",
        "PartyChangedOrder",
        "PartyPlayerLeft",
        "PartyLeaderChanged",
        "PartyLootSettingChangedPlayer",
        "PartySilverGained",
        "PartyPlayerUpdated",
        "PartyInvitationPlayerBusy",
        "SpellCooldownUpdate",
        "NewHellgate",
        "NewHellgateExit",
        "NewExpeditionExit",
        "NewExpeditionNarrator",
        "ExitEnterStart",
        "ExitEnterCancel",
        "ExitEnterFinished",
        "HellClusterTimeUpdate",
        "NewAgent",
        "FullQuestInfo",
        "QuestProgressInfo",
        "FullExpeditionInfo",
        "ExpeditionQuestProgressInfo",
        "InvitedToExpedition",
        "ExpeditionRegistrationInfo",
        "EnteringExpeditionStart",
        "EnteringExpeditionCancel",
        "RewardGranted",
        "ArenaRegistrationInfo",
        "EnteringArenaStart",
        "EnteringArenaCancel",
        "EnteringArenaLockStart",
        "EnteringArenaLockCancel",
        "InvitedToArenaMatch",
        "PlayerCounts",
        "InCombatStateUpdate",
        "OtherGrabbedLoot",
        "SiegeCampClaimStart",
        "SiegeCampClaimCancel",
        "SiegeCampClaimFinished",
        "SiegeCampScheduleResult",
        "TreasureChestUsingStart",
        "TreasureChestUsingFinished",
        "TreasureChestUsingCancel",
        "TreasureChestUsingOpeningComplete",
        "TreasureChestForceCloseInventory",
        "PremiumChanged",
        "PremiumExtended",
        "PremiumLifeTimeRewardGained",
        "LaborerGotUpgraded",
        "JournalGotFull",
        "JournalFillError",
        "FriendRequest",
        "FriendRequestInfos",
        "FriendInfos",
        "FriendRequestAnswered",
        "FriendOnlineStatus",
        "FriendRequestCanceled",
        "FriendRemoved",
        "FriendUpdated",
        "PartyLootItems",
        "PartyLootItemsRemoved",
        "ReputationUpdate",
        "DefenseUnitAttackBegin",
        "DefenseUnitAttackEnd",
        "DefenseUnitAttackDamage",
        "UnrestrictedPvpZoneUpdate",
        "ReputationImplicationUpdate",
        "NewMountObject",
        "MountHealthUpdate",
        "MountCooldownUpdate",
        "NewExpeditionAgent",
        "NewExpeditionCheckPoint",
        "ExpeditionStartEvent",
        "VoteEvent",
        "RatingEvent",
        "NewArenaAgent",
        "BoostFarmable",
        "UseFunction",
        "NewPortalEntrance",
        "NewPortalExit",
        "WaitingQueueUpdate",
        "PlayerMovementRateUpdate",
        "ObserveStart",
        "MinimapZergs",
        "PaymentTransactions",
        "PerformanceStatsUpdate",
        "OverloadModeUpdate",
        "DebugDrawEvent",
        "RecordCameraMove",
        "RecordStart",
        "TerritoryClaimStart",
        "TerritoryClaimCancel",
        "TerritoryClaimFinished",
        "TerritoryScheduleResult",
        "UpdateAccountState",
        "StartDeterministicRoam",
        "GuildFullAccessTagsUpdated",
        "GuildAccessTagUpdated",
        "GvgSeasonUpdate",
        "GvgSeasonCheatCommand",
        "SeasonPointsByKillingBooster",
        "FishingStart",
        "FishingCast",
        "FishingCatch",
        "FishingFinished",
        "FishingCancel",
        "NewFloatObject",
        "NewFishingZoneObject",
        "FishingMiniGame",
        "SteamAchievementCompleted",
        "UpdatePuppet",
        "ChangeFlaggingFinished",
        "NewOutpostObject",
        "OutpostUpdate",
        "OutpostClaimed",
        "OverChargeEnd",
        "OverChargeStatus",
        "OutpostReward",
        "NewUnlockedPersonalSeasonRewards" , 
        "PersonalSeasonPointsGained"
            }
            );

        public enum EventCodes_Enum : int
        {   
            None = 0,
            Leave = 1,
            JoinFinished = 2,
            Move = 3,
            Teleport = 4,
            ChangeEquipment = 5,
            HealthUpdate = 6,
            EnergyUpdate = 7,
            DamageShieldUpdate = 8,
            CraftingFocusUpdate = 9,
            ActiveSpellEffectsUpdate = 10, // 0x000A
            ResetCooldowns = 11, // 0x000B
            Attack = 12, // 0x000C
            CastStart = 13, // 0x000D
            CastCancel = 14, // 0x000E
            CastTimeUpdate = 15, // 0x000F
            CastFinished = 16, // 0x0010
            CastSpell = 17, // 0x0011
            CastHit = 18, // 0x0012
            ChannelingEnded = 19, // 0x0013
            AttackBuilding = 20, // 0x0014
            InventoryPutItem = 21, // 0x0015
            InventoryDeleteItem = 22, // 0x0016
            NewCharacter = 23, // 0x0017
            NewEquipmentItem = 24, // 0x0018
            NewSimpleItem = 25, // 0x0019
            NewFurnitureItem = 26, // 0x001A
            NewJournalItem = 27, // 0x001B
            NewSimpleHarvestableObject = 28, // 0x001C
            NewSimpleHarvestableObjectList = 29, // 0x001D
            NewHarvestableObject = 30, // 0x001E
            NewSilverObject = 31, // 0x001F
            NewBuilding = 32, // 0x0020
            HarvestableChangeState = 33, // 0x0021
            MobChangeState = 34, // 0x0022
            FactionBuildingInfo = 35, // 0x0023
            CraftBuildingInfo = 36, // 0x0024
            RepairBuildingInfo = 37, // 0x0025
            MeldBuildingInfo = 38, // 0x0026
            ConstructionSiteInfo = 39, // 0x0027
            PlayerBuildingInfo = 40, // 0x0028
            FarmBuildingInfo = 41, // 0x0029
            LaborerObjectInfo = 42, // 0x002A
            LaborerObjectJobInfo = 43, // 0x002B
            MarketPlaceBuildingInfo = 44, // 0x002C
            HarvestStart = 45, // 0x002D
            HarvestCancel = 46, // 0x002E
            HarvestFinished = 47, // 0x002F
            TakeSilver = 48, // 0x0030
            ActionOnBuildingStart = 49, // 0x0031
            ActionOnBuildingCancel = 50, // 0x0032
            ActionOnBuildingFinished = 51, // 0x0033
            ItemRerollQualityStart = 52, // 0x0034
            ItemRerollQualityCancel = 53, // 0x0035
            ItemRerollQualityFinished = 54, // 0x0036
            InstallResourceStart = 55, // 0x0037
            InstallResourceCancel = 56, // 0x0038
            InstallResourceFinished = 57, // 0x0039
            CraftItemFinished = 58, // 0x003A
            LogoutCancel = 59, // 0x003B
            ChatMessage = 60, // 0x003C
            ChatSay = 61, // 0x003D
            ChatWhisper = 62, // 0x003E
            PlayEmote = 63, // 0x003F
            StopEmote = 64, // 0x0040
            SystemMessage = 65, // 0x0041
            UpdateMoney = 66, // 0x0042
            UpdateFame = 67, // 0x0043
            UpdateLearningPoints = 68, // 0x0044
            UpdateReSpecPoints = 69, // 0x0045
            UpdateCurrency = 70, // 0x0046
            UpdateFactionStanding = 71, // 0x0047
            Respawn = 72, // 0x0048
            ServerDebugLog = 73, // 0x0049
            CharacterEquipmentChanged = 74, // 0x004A
            RegenerationHealthChanged = 75, // 0x004B
            RegenerationEnergyChanged = 76, // 0x004C
            RegenerationMountHealthChanged = 77, // 0x004D
            RegenerationCraftingChanged = 78, // 0x004E
            RegenerationHealthEnergyComboChanged = 79, // 0x004F
            RegenerationPlayerComboChanged = 80, // 0x0050
            DurabilityChanged = 81, // 0x0051
            NewLoot = 82, // 0x0052
            ContainerInfo = 83, // 0x0053
            GuildVaultInfo = 84, // 0x0054
            GuildUpdate = 85, // 0x0055
            GuildPlayerUpdated = 86, // 0x0056
            InvitedToGuild = 87, // 0x0057
            GuildMemberWorldUpdate = 88, // 0x0058
            UpdateMatchDetails = 89, // 0x0059
            ObjectEvent = 90, // 0x005A
            NewMonolithObject = 91, // 0x005B
            NewSiegeCampObject = 92, // 0x005C
            NewOrbObject = 93, // 0x005D
            NewCastleObject = 94, // 0x005E
            NewSpellEffectArea = 95, // 0x005F
            NewChainSpell = 96, // 0x0060
            UpdateChainSpell = 97, // 0x0061
            NewTreasureChest = 98, // 0x0062
            StartMatch = 99, // 0x0063
            StartTerritoryMatchInfos = 100, // 0x0064
            StartArenaMatchInfos = 101, // 0x0065
            EndTerritoryMatch = 102, // 0x0066
            EndArenaMatch = 103, // 0x0067
            MatchUpdate = 104, // 0x0068
            ActiveMatchUpdate = 105, // 0x0069
            NewMob = 106, // 0x006A
            DebugAggroInfo = 107, // 0x006B
            DebugVariablesInfo = 108, // 0x006C
            DebugReputationInfo = 109, // 0x006D
            DebugDiminishingReturnInfo = 110, // 0x006E
            ClaimOrbStart = 111, // 0x006F
            ClaimOrbFinished = 112, // 0x0070
            ClaimOrbCancel = 113, // 0x0071
            OrbUpdate = 114, // 0x0072
            OrbClaimed = 115, // 0x0073
            NewWarCampObject = 116, // 0x0074
            GuildMemberTerritoryUpdate = 117, // 0x0075
            InvitedMercenaryToMatch = 118, // 0x0076
            ClusterInfoUpdate = 119, // 0x0077
            ForcedMovement = 120, // 0x0078
            ForcedMovementCancel = 121, // 0x0079
            CharacterStats = 122, // 0x007A
            CharacterStatsKillHistory = 123, // 0x007B
            CharacterStatsDeathHistory = 124, // 0x007C
            GuildStats = 125, // 0x007D
            KillHistoryDetails = 126, // 0x007E
            FullAchievementInfo = 127, // 0x007F
            FinishedAchievement = 128, // 0x0080
            AchievementProgressInfo = 129, // 0x0081
            FullAchievementProgressInfo = 130, // 0x0082
            FullTrackedAchievementInfo = 131, // 0x0083
            AgentQuestOffered = 132, // 0x0084
            AgentDebugInfo = 133, // 0x0085
            ConsoleEvent = 134, // 0x0086
            TimeSync = 135, // 0x0087
            ChangeAvatar = 136, // 0x0088
            GameEvent = 137, // 0x0089
            KilledPlayer = 138, // 0x008A
            Died = 139, // 0x008B
            KnockedDown = 140, // 0x008C
            MatchPlayerJoinedEvent = 141, // 0x008D
            MatchPlayerStatsEvent = 142, // 0x008E
            MatchPlayerStatsCompleteEvent = 143, // 0x008F
            MatchTimeLineEventEvent = 144, // 0x0090
            MatchPlayerMainGearStatsEvent = 145, // 0x0091
            MatchPlayerChangedAvatarEvent = 146, // 0x0092
            InvitationPlayerTrade = 147, // 0x0093
            PlayerTradeStart = 148, // 0x0094
            PlayerTradeCancel = 149, // 0x0095
            PlayerTradeUpdate = 150, // 0x0096
            PlayerTradeFinished = 151, // 0x0097
            PlayerTradeAcceptChange = 152, // 0x0098
            MiniMapPing = 153, // 0x0099
            MinimapPlayerPositions = 154, // 0x009A
            MarketPlaceNotification = 155, // 0x009B
            DuellingChallengePlayer = 156, // 0x009C
            NewDuellingPost = 157, // 0x009D
            DuelStarted = 158, // 0x009E
            DuelEnded = 159, // 0x009F
            DuelDenied = 160, // 0x00A0
            DuelLeftArea = 161, // 0x00A1
            DuelReEnteredArea = 162, // 0x00A2
            NewRealEstate = 163, // 0x00A3
            MiniMapOwnedBuildingsPositions = 164, // 0x00A4
            RealEstateListUpdate = 165, // 0x00A5
            GuildLogoUpdate = 166, // 0x00A6
            PlaceableItemPlace = 167, // 0x00A7
            PlaceableItemPlaceCancel = 168, // 0x00A8
            FurnitureObjectBuffProviderInfo = 169, // 0x00A9
            FurnitureObjectCheatProviderInfo = 170, // 0x00AA
            FarmableObjectInfo = 171, // 0x00AB
            LaborerObjectPlace = 172, // 0x00AC
            LaborerObjectPlaceCancel = 173, // 0x00AD
            NewUnreadMails = 174, // 0x00AE
            GuildLogoObjectUpdate = 175, // 0x00AF
            StartLogout = 176, // 0x00B0
            NewChatChannels = 177, // 0x00B1
            JoinedChatChannel = 178, // 0x00B2
            LeftChatChannel = 179, // 0x00B3
            RemovedChatChannel = 180, // 0x00B4
            AccessStatus = 181, // 0x00B5
            Mounted = 182, // 0x00B6
            MountCancel = 183, // 0x00B7
            NewTravelpoint = 184, // 0x00B8
            NewIslandAccessPoint = 185, // 0x00B9
            NewExit = 186, // 0x00BA
            UpdateHome = 187, // 0x00BB
            UpdateChatSettings = 188, // 0x00BC
            ResurrectionOffer = 189, // 0x00BD
            ResurrectionReply = 190, // 0x00BE
            LootEquipmentChanged = 191, // 0x00BF
            UpdateUnlockedGuildLogos = 192, // 0x00C0
            UpdateUnlockedAvatars = 193, // 0x00C1
            UpdateUnlockedAvatarRings = 194, // 0x00C2
            UpdateUnlockedBuildings = 195, // 0x00C3
            DailyLoginBonus = 196, // 0x00C4
            NewIslandManagement = 197, // 0x00C5
            NewTeleportStone = 198, // 0x00C6
            Cloak = 199, // 0x00C7
            PartyInvitation = 200, // 0x00C8
            PartyJoined = 201, // 0x00C9
            PartyDisbanded = 202, // 0x00CA
            PartyPlayerJoined = 203, // 0x00CB
            PartyChangedOrder = 204, // 0x00CC
            PartyPlayerLeft = 205, // 0x00CD
            PartyLeaderChanged = 206, // 0x00CE
            PartyLootSettingChangedPlayer = 207, // 0x00CF
            PartySilverGained = 208, // 0x00D0
            PartyPlayerUpdated = 209, // 0x00D1
            PartyInvitationPlayerBusy = 210, // 0x00D2
            SpellCooldownUpdate = 211, // 0x00D3
            NewHellgate = 212, // 0x00D4
            NewHellgateExit = 213, // 0x00D5
            NewExpeditionExit = 214, // 0x00D6
            NewExpeditionNarrator = 215, // 0x00D7
            ExitEnterStart = 216, // 0x00D8
            ExitEnterCancel = 217, // 0x00D9
            ExitEnterFinished = 218, // 0x00DA
            HellClusterTimeUpdate = 219, // 0x00DB
            NewAgent = 220, // 0x00DC
            FullQuestInfo = 221, // 0x00DD
            QuestProgressInfo = 222, // 0x00DE
            FullExpeditionInfo = 223, // 0x00DF
            ExpeditionQuestProgressInfo = 224, // 0x00E0
            InvitedToExpedition = 225, // 0x00E1
            ExpeditionRegistrationInfo = 226, // 0x00E2
            EnteringExpeditionStart = 227, // 0x00E3
            EnteringExpeditionCancel = 228, // 0x00E4
            RewardGranted = 229, // 0x00E5
            ArenaRegistrationInfo = 230, // 0x00E6
            EnteringArenaStart = 231, // 0x00E7
            EnteringArenaCancel = 232, // 0x00E8
            EnteringArenaLockStart = 233, // 0x00E9
            EnteringArenaLockCancel = 234, // 0x00EA
            InvitedToArenaMatch = 235, // 0x00EB
            PlayerCounts = 236, // 0x00EC
            InCombatStateUpdate = 237, // 0x00ED
            OtherGrabbedLoot = 238, // 0x00EE
            SiegeCampClaimStart = 239, // 0x00EF
            SiegeCampClaimCancel = 240, // 0x00F0
            SiegeCampClaimFinished = 241, // 0x00F1
            SiegeCampScheduleResult = 242, // 0x00F2
            TreasureChestUsingStart = 243, // 0x00F3
            TreasureChestUsingFinished = 244, // 0x00F4
            TreasureChestUsingCancel = 245, // 0x00F5
            TreasureChestUsingOpeningComplete = 246, // 0x00F6
            TreasureChestForceCloseInventory = 247, // 0x00F7
            PremiumChanged = 248, // 0x00F8
            PremiumExtended = 249, // 0x00F9
            PremiumLifeTimeRewardGained = 250, // 0x00FA
            LaborerGotUpgraded = 251, // 0x00FB
            JournalGotFull = 252, // 0x00FC
            JournalFillError = 253, // 0x00FD
            FriendRequest = 254, // 0x00FE
            FriendRequestInfos = 255, // 0x00FF
            FriendInfos = 256, // 0x0100
            FriendRequestAnswered = 257, // 0x0101
            FriendOnlineStatus = 258, // 0x0102
            FriendRequestCanceled = 259, // 0x0103
            FriendRemoved = 260, // 0x0104
            FriendUpdated = 261, // 0x0105
            PartyLootItems = 262, // 0x0106
            PartyLootItemsRemoved = 263, // 0x0107
            ReputationUpdate = 264, // 0x0108
            DefenseUnitAttackBegin = 265, // 0x0109
            DefenseUnitAttackEnd = 266, // 0x010A
            DefenseUnitAttackDamage = 267, // 0x010B
            UnrestrictedPvpZoneUpdate = 268, // 0x010C
            ReputationImplicationUpdate = 269, // 0x010D
            NewMountObject = 270, // 0x010E
            MountHealthUpdate = 271, // 0x010F
            MountCooldownUpdate = 272, // 0x0110
            NewExpeditionAgent = 273, // 0x0111
            NewExpeditionCheckPoint = 274, // 0x0112
            ExpeditionStartEvent = 275, // 0x0113
            VoteEvent = 276, // 0x0114
            RatingEvent = 277, // 0x0115
            NewArenaAgent = 278, // 0x0116
            BoostFarmable = 279, // 0x0117
            UseFunction = 280, // 0x0118
            NewPortalEntrance = 281, // 0x0119
            NewPortalExit = 282, // 0x011A
            WaitingQueueUpdate = 283, // 0x011B
            PlayerMovementRateUpdate = 284, // 0x011C
            ObserveStart = 285, // 0x011D
            MinimapZergs = 286, // 0x011E
            PaymentTransactions = 287, // 0x011F
            PerformanceStatsUpdate = 288, // 0x0120
            OverloadModeUpdate = 289, // 0x0121
            DebugDrawEvent = 290, // 0x0122
            RecordCameraMove = 291, // 0x0123
            RecordStart = 292, // 0x0124
            TerritoryClaimStart = 293, // 0x0125
            TerritoryClaimCancel = 294, // 0x0126
            TerritoryClaimFinished = 295, // 0x0127
            TerritoryScheduleResult = 296, // 0x0128
            UpdateAccountState = 297, // 0x0129
            StartDeterministicRoam = 298, // 0x012A
            GuildFullAccessTagsUpdated = 299, // 0x012B
            GuildAccessTagUpdated = 300, // 0x012C
            GvgSeasonUpdate = 301, // 0x012D
            GvgSeasonCheatCommand = 302, // 0x012E
            SeasonPointsByKillingBooster = 303, // 0x012F
            FishingStart = 304, // 0x0130
            FishingCast = 305, // 0x0131
            FishingCatch = 306, // 0x0132
            FishingFinished = 307, // 0x0133
            FishingCancel = 308, // 0x0134
            NewFloatObject = 309, // 0x0135
            NewFishingZoneObject = 310, // 0x0136
            FishingMiniGame = 311, // 0x0137
            SteamAchievementCompleted = 312, // 0x0138
            UpdatePuppet = 313, // 0x0139
            ChangeFlaggingFinished = 314, // 0x013A
            NewOutpostObject = 315, // 0x013B
            OutpostUpdate = 316, // 0x013C
            OutpostClaimed = 317, // 0x013D
            OverChargeEnd = 318, // 0x013E
            OverChargeStatus = 319, // 0x013F
            OutpostReward = 320, // 0x0140
            NewUnlockedPersonalSeasonRewards = 321, // 0x0141
            PersonalSeasonPointsGained = 322, // 0x0142
        }

    }
    
}
