﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AlbionPacketParser
{
    class SpellAnalyzer
    {
        XmlDocument doc = new XmlDocument();

        public SkillPosition GetSkillPosition(string equipment, string slot = "none")
        {
            switch(equipment)
            {
                case "WEAPON SPELLS":
                    if (slot == "SLOT 1")
                    {
                        return SkillPosition.WeaponOne;
                    }

                    else if (slot == "SLOT 2")
                    {
                        return SkillPosition.WeaponTwo;
                    }

                    else
                    {
                        return SkillPosition.WeaponThree;
                    }
                case "ARMOR SPELLS":
                    return SkillPosition.Chest;
                case "HELMET SPELLS":
                    return SkillPosition.Helmet;
                case "SHOE SPELLS":
                    return SkillPosition.Boots;
                default:
                    return SkillPosition.Food;
            }
        }

        public SpellAnalyzer()
        {

            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.IgnoreComments = false;
            readerSettings.IgnoreWhitespace = true;
            XmlReader reader = XmlReader.Create("spells.xml", readerSettings);

            string outputData = "";

            int i = 0;

            string name;

            string[] lastKnownName = { "" } ;

            string equipment = "";
            string slot = "";

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    ///Any comments not being read, it has something to do with having a comment above it
                    ///Apparently only comments at the top of consecutive comments will be read
                    ///you will need to make sure there are no comments before them or find a way to read the entire comment
                    ///used <ignore></ignore> tag before by finding and replacing the
                    ///comments that had something before them IE:
                    ///<!-- Some comment before SLOT 1 -->
                    ///<ignore></ignore>
                    ///<!-- *** SLOT 1 *** -->

                    //Console.WriteLine(reader.Value);
                    if (reader.Value.Contains("WEAPON SPELLS"))
                        //Console.WriteLine(reader.Value);
                        equipment = "WEAPON SPELLS";
                    if (reader.Value.Contains("SLOT 1"))
                        //Console.WriteLine("SLOT 1");
                        slot = "SLOT 1";
                    if (reader.Value.Contains("SLOT 2"))
                        //Console.WriteLine("SLOT 2");
                        slot = "SLOT 2";
                    if (reader.Value.Contains("SLOT 3"))
                        //Console.WriteLine("SLOT 3");
                        slot = "SLOT 3";
                    if (reader.Value.Contains("ARMOR SPELLS"))
                        //Console.WriteLine(reader.Value + " | " + i);
                        equipment = "ARMOR SPELLS";
                    if (reader.Value.Contains("HELMET SPELLS"))
                        //Console.WriteLine(reader.Value + " | " + i);
                        equipment = "HELMET SPELLS";
                    if (reader.Value.Contains("SHOE SPELLS"))
                        //Console.WriteLine(reader.Value + " | " + i);
                        equipment = "SHOE SPELLS";
                }

                if (reader.IsStartElement())
                {
                    switch (reader.Name)
                    {
                        case "passivespell":
                            name = reader["uniquename"];
                            //Console.WriteLine("PASSIVE: " + name + " ID: " + i);
                            i += 1;
                            break;
                        case "activespell":
                            name = reader["uniquename"];
                            if (i > 1040 && i < 1689) //filter only the skills within range that we want
                            {
                                string[] splitName = name.Split('_');

                                if (lastKnownName[0] != splitName[0])
                                {
                                    lastKnownName = splitName;
                                    //Console.WriteLine($"{name}, {reader["recastdelay"]}, {i}, {GetSkillPosition(equipment, slot)}");
                                    outputData += ($"{name}, {reader["recastdelay"]}, {i}, {GetSkillPosition(equipment, slot)}" + Environment.NewLine);
                                    
                                    //GetAndSaveIcon(name);
                                }
                            }
                            i += 1;
                            break;
                        case "togglespell":
                            name = reader["uniquename"];
                            //Console.WriteLine("ACTIVE: " + name + " ID: " + (i));
                            //GetAndSaveIcon(name);
                            i += 1;
                            break;
                        case "channelingspell":
                            //Console.WriteLine("CHANNEL ID: " + (i));
                            i += 1;
                            break;
                    }
                }
            }

            Console.WriteLine(outputData);
            SkillIDs.LoadSkillsFromFile(outputData);
            //SkillIDs.SaveSkillsToFile();

        }

        public System.Drawing.Image DownloadImageFromUrl(string imageUrl)
        {
            System.Drawing.Image image = null;

            try
            {
                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                System.Net.WebResponse webResponse = webRequest.GetResponse();

                System.IO.Stream stream = webResponse.GetResponseStream();

                image = System.Drawing.Image.FromStream(stream);

                webResponse.Close();
            }
            catch (Exception ex)
            {
                return null;
            }

            return image;
        }

        public void GetAndSaveIcon(string name)
        {
            string url = "https://gameinfo.albiononline.com/api/gameinfo/spells/" + name + ".png";
            Console.WriteLine(url);
            string path = "Resources/icons/" + name + ".png";
            System.Drawing.Image image = DownloadImageFromUrl(url);//client.DownloadFile(new Uri(url), path);
            Console.WriteLine(name + " icon download success");                  
            image.Save(path);
        }
    }
}
