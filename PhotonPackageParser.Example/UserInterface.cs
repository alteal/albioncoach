﻿using System;
using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using AlbionPacketParser;

namespace PhotonPackageParser.Program
{


    public partial class UserInterface : MetroFramework.Forms.MetroForm
    {
        // Create instance of the Userinterface
        //public static UserInterface UIForm = new UserInterface();
     public   bool closestream;




        private IPhotonPackageHandler _photonHandler;
        private PhotonPackageParser _photonPackageParser;
        
        public UserInterface()
        {
            InitializeComponent();
            
        }

        public void UserInterface_Load(object sender, EventArgs e)
        {
            // Initiate the Packetsniffing and Handling on another thread
            Thread WorkerThread = new Thread(() => Start());
            WorkerThread.IsBackground = true;
            WorkerThread.Start();


            Overlay _overlay = new Overlay();
            _overlay.Show();
        }






      public  void Debug(string type, int Eventcode, int Operationcode, Dictionary<byte, object> parameters)
      {

            

            if (Eventcode > 0 && Operationcode == 0) { ConsoleTbx.AppendText("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode + Environment.NewLine); }
            else if (Operationcode > 0 && Eventcode == 0) { ConsoleTbx.AppendText(type + ": " + OperationCodes.OperationToString[Operationcode] + "  " + Operationcode + Environment.NewLine); }
            
            //if (saved == false) { saved = true; } else { return; }
            //FileStream writerFileStream =
            //     new FileStream("TestFile.dat", FileMode.Create, FileAccess.Write);
            // Save our dictionary of friends to file
            //formatter.Serialize(writerFileStream, parameters);
            //MessageBox.Show("Im still working");
            foreach (KeyValuePair<byte, object> kvp in parameters)
            {
                ///try
                //{

                    // StringArray
                    if (kvp.Value is string[] StringArray) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < StringArray.Length; i++) { ConsoleTbx.AppendText(i + " : " + StringArray[i] + Environment.NewLine); } }

                // Guid
                //else if (kvp.Value is Guid _Guid) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); ConsoleTbx.AppendText(_Guid.ToString() + Environment.NewLine); }

                // Guid Array
                else if (kvp.Value is Guid[] _GuidArray) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < _GuidArray.Length; i++) { ConsoleTbx.AppendText(i + " : " + _GuidArray[i].ToString() + Environment.NewLine); } }

                // Int32 Array
                else if (kvp.Value is Int16[] Int16Array1) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int16Array1.Length; i++) { ConsoleTbx.AppendText(i + " : " + Int16Array1[i] + Environment.NewLine); } }
                else if (kvp.Value is Int16[][] Int16Array2) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int16Array2.Length; i++) { ConsoleTbx.AppendText(i + " : " + Int16Array2[i] + Environment.NewLine); } }


                // Int32 Array
                else if (kvp.Value is Int32[] Int32Array1) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int32Array1.Length; i++) { ConsoleTbx.AppendText(i + " : " + Int32Array1[i] + Environment.NewLine); } }
                else if (kvp.Value is Int32[][] Int32Array2) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int32Array2.Length; i++) { ConsoleTbx.AppendText(i + " : " + Int32Array2[i] + Environment.NewLine); } }

                // Int64 Array
                else if (kvp.Value is Int64[] Int64Array1) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int64Array1.Length; i++) { ConsoleTbx.AppendText(i + " : " + Int64Array1[i] + Environment.NewLine); } }
                else if (kvp.Value is Int64[][] Int64Array2) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int64Array2.Length; i++) { ConsoleTbx.AppendText(i + " : " + Int64Array2[i] + Environment.NewLine); } }
                              
                // Most of the time Byte[] and Byte[][] are GUID and GUID arrays.
                // Sometimes they are not. This piece checks if it is possibly a GUID, if yes then it casts it as a GUID and continues with it.
                // If not, it will write the bytes withing the array.
                // Byte Array 
                else if (kvp.Value is Byte[] ByteArray1) {   ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); if (ByteArray1.Length == 16) { Guid _guid = new Guid(ByteArray1); ConsoleTbx.AppendText(_guid.ToString() + Environment.NewLine); } else { for (int i = 0; i < ByteArray1.Length; i++) { ConsoleTbx.AppendText(i + " : " + ByteArray1[i] + Environment.NewLine); } } }
                else if (kvp.Value is byte[][] ByteArray2) { if (ByteArray2.Length ==0) { return; }   ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value +  Environment.NewLine); if (ByteArray2[0].Length == 16) { List<byte[]> GuidList = ByteArray2.Cast<byte[]>().ToList(); for (int i = 0; i < GuidList.Count; i++) { Guid _Guid = new Guid(GuidList[i]); ConsoleTbx.AppendText(_Guid.ToString() + Environment.NewLine); } } else { if (ByteArray2.Length == 0 || ByteArray2[0].Length == 0) { ConsoleTbx.AppendText("Empty Byte[][]" + Environment.NewLine); goto SKIP; } else { for (int i = 0; i < ByteArray2.Length; i++) {  ConsoleTbx.AppendText(i + "  "); for (int j = 0; j < ByteArray2[i].Length; j++) {  ConsoleTbx.AppendText(ByteArray2[i][j].ToString()); } ConsoleTbx.AppendText(Environment.NewLine); } } } } // ByteArray2[i]

                    // Single Array
                else if (kvp.Value is Single[] SingleArray1) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < SingleArray1.Length; i++) { ConsoleTbx.AppendText(i + " : " + SingleArray1[i] + Environment.NewLine); } }
                else if (kvp.Value is Single[][] SingleArray2) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < SingleArray2.Length; i++) { ConsoleTbx.AppendText(i + " : " + SingleArray2[i] + Environment.NewLine); } }

                // Dictionary
                else if (kvp.Value is Dictionary<byte, byte> Dict) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, byte> _kvp in Dict) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
                else if (kvp.Value is Dictionary<byte, string> Dict_Byte_String) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, string> _kvp in Dict_Byte_String) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
                else if (kvp.Value is Dictionary<byte, Int32> Dict_Byte_Int32) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, Int32> _kvp in Dict_Byte_Int32) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
                else if (kvp.Value is Dictionary<byte, Int64> Dict_byte_Int64) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, Int64> _kvp in Dict_byte_Int64) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
                else if (kvp.Value is Dictionary<Int32, Int64> Dict_Int32_Int64) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<Int32, Int64> _kvp in Dict_Int32_Int64) { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
                else { ConsoleTbx.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); }
                    
                    //}
                    //catch (Exception E) { MessageBox.Show("ii = "+ii+Environment.NewLine +"jj = "+jj+Environment.NewLine + E.Message + Environment.NewLine + len1 + "    "+len2); }
                SKIP:;
            }

       
      }

        public void PacketHandler(Packet packet) // Callback function when A packet is received.
        {
            IpV4Datagram ip = packet.Ethernet.IpV4;
            UdpDatagram udp = ip.Udp;

            if (udp == null || (udp.SourcePort != 5056 && udp.DestinationPort != 5056))
            // Checking if the packet is valid and comes from the right ports.
            {
                return;
            }

            _photonPackageParser.DeserializeMessageAndCallback(udp); // Give the UDP datagram to the deserialiser.
        }

        private void Start()
        {
            var device = DeviceSelector.AskForPacketDevice(); // get Network Device
            // var device = new OfflinePacketDevice("dump.pcap"); // Your wireshark dump (IT MUST BE *.pcap)
            _photonHandler = new PackageHandler();
            _photonPackageParser = new PhotonPackageParser(_photonHandler);




            using (PacketCommunicator communicator = device.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 1000))
            {
                communicator.ReceivePackets(0, PacketHandler); // Initiate the never ending listener
            }
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            closestream = true;
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            Overlay _overlay = new Overlay();
            _overlay.Show();
        }
    }
}
