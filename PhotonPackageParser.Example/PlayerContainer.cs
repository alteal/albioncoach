﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using PhotonPackageParser.Program;
using System;
using System.Drawing.Drawing2D;

namespace AlbionPacketParser
{
    //Could show in new window with all click through
    //To show in a new window just set form or this.ShowInTaskbar = false;
    //use code to make everything click through
    public class Alert
    {
        public string SkillName;
        public string AlertText;
        public int MaxFontSize;
        public int MinFontSize;
        private int currentFontSize;
        public Color Color;

        public void Animate()
        {

        }

        public void Dispose()
        {

        }
    }

    public class ItemIdEditor
    {
        int changeAllModifier = 0;
        int changeIndividualModifier = 0;
        int idToChange = 0;
    }

    public class PlayerRegister
    {
        //Can track healing by determining last health known - new health known = total healed, overhealed = heal amount - total healed
        public Label totalDamageLabel;
        public List<PlayerContainer> playerContainers = new List<PlayerContainer>();
        public List<Button> swapButtons = new List<Button>();
        public Point Location;
        public int VerticalPlayerContainerOffset = 110;
        private Control parent;
        public PictureBox MoveIcon;
        public bool dragging;
        private int xPos;
        private int yPos;
        private PlayerRegister oppositeRegister;
        private Action<PlayerRegister, PlayerRegister, PlayerContainer> switchCallback;
        private Action<Point> settingsCallback;
        private bool lockPosition = false;

        public PlayerRegister(Point pos, Control parent, Action<Point> settingsCallback)
        {
            Location = pos;
            this.parent = parent;
            this.settingsCallback = settingsCallback;
            MoveIcon = new PictureBox();
            MoveIcon.ImageLocation = "./Resources/moveIcon.png";
            MoveIcon.Location = new Point(Location.X - 10, Location.Y - 25);
            MoveIcon.Size = new Size(20, 20);
            MoveIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            MoveIcon.Parent = parent;
            MoveIcon.MouseMove += MouseMove;
            MoveIcon.MouseUp += MouseUp;
            MoveIcon.MouseDown += MouseDown;

        }

        public void UseOppositeRegister(PlayerRegister register, Action<PlayerRegister, PlayerRegister, PlayerContainer> switchCallback)
        {
            oppositeRegister = register;
            this.switchCallback = switchCallback;
        }

        public void AddSwapButton(PlayerContainer playerContainer)
        {
            // set font with Font Class and the returned Size from NewFontSize();
            Font f = new Font("Leelawadee UI", 12, FontStyle.Bold);

            Button swapButton = new Button();
            swapButton.Image = Image.FromFile("./Resources/swapArrow.png");
            swapButton.FlatStyle = FlatStyle.Flat;
            swapButton.Size = new Size(24, 13);
            swapButton.BackColor = Color.White;
            swapButton.Location = new Point(playerContainer.Location.X - (swapButton.Size.Width + 5), playerContainer.Location.Y + 5);
            swapButton.Parent = parent;
            swapButton.Click += MovePlayerContainerToOppositeRegister;
            swapButtons.Add(swapButton);
        }

        public void MovePlayerContainerToOppositeRegister(object sender, EventArgs e)
        {
            int index = swapButtons.IndexOf((Button)sender);
            Button swapButton = (Button)sender;
            PlayerContainer playerContainer = playerContainers[index];
            oppositeRegister.AddSwapButton(playerContainer);
            switchCallback(this, oppositeRegister, playerContainer);
            parent.Controls.Remove(swapButton);
            swapButton.Dispose();
            swapButtons.RemoveAt(index);
            RepostionPlayerContainers();
            oppositeRegister.RepostionPlayerContainers();
        }

        public void RepostionPlayerContainers()
        {
            for (int i = 0; i < playerContainers.Count; i++)
            {
                PlayerContainer playerContainer = playerContainers[i];
                playerContainer.Reposition(new Point(Location.X, Location.Y + i * VerticalPlayerContainerOffset + playerContainer.healLabelSpacer));

                Button swapButton = swapButtons[i];
                swapButton.Location = new Point(playerContainer.Location.X - (swapButton.Size.Width + 5), playerContainer.Location.Y + 5);
            }
        }

        public void AddPlayerContainer(string playerID, int timeElapsed)
        {
            playerContainers.Add(new PlayerContainer(playerID, timeElapsed, parent, new Point(Location.X, Location.Y + playerContainers.Count * VerticalPlayerContainerOffset), this));

            if (oppositeRegister != null)
            {
                Console.WriteLine("Attempting to add swap button");
                AddSwapButton(playerContainers[playerContainers.Count - 1]);            
            }
        }

        public bool ExistsInPlayerContainer(string playerID, out int indexExistsAt)
        {
            bool exists = false;
            indexExistsAt = 0;
            for (int i = 0; i <playerContainers.Count; i++)
            {
                if (playerContainers[i].CasterID == playerID)
                {
                    indexExistsAt = i;
                    exists = true;
                    return exists;
                }
            }
            return exists;
        }

        public bool ExistsInPlayerContainer(string playerID)
        {
            bool exists = false;
            for (int i = 0; i < playerContainers.Count; i++)
            {
                if (playerContainers[i].CasterID == playerID)
                {
                    exists = true;
                    return exists;
                }
            }
            return exists;
        }

        public void Dispose()
        {
            foreach (PlayerContainer playerContainer in playerContainers)
            {
                playerContainer.Dispose();
            }

            foreach (Button button in swapButtons)
            {
                parent.Controls.Remove(button);
                button.Dispose();
            }

            swapButtons.Clear();
            playerContainers.Clear();
        }

        public void MouseUp(object sender, MouseEventArgs args)
        {
            var c = sender as PictureBox;
            if (null == c) return;
            dragging = false;
            settingsCallback(Location);
            //DumpItemDataToFile();
        }

        public void MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            dragging = true;
            xPos = e.X;
            yPos = e.Y;
        }

        public void ToggleLockPositions()
        {
            if (!lockPosition)
            {
                lockPosition = true;
                MoveIcon.Hide();
            }           
            else
            {
                lockPosition = false;
                MoveIcon.Show();
            }
        }

        public void MouseMove(object sender, MouseEventArgs e)
        {
            var c = sender as PictureBox;
            if (!dragging || null == c || lockPosition) return;
            c.Top = e.Y + c.Top - yPos;
            c.Left = e.X + c.Left - xPos;
            Location.X = c.Left + 5;
            Location.Y = c.Top + 25;
            RepostionPlayerContainers();
        }

        //public void UpdateAllComponentLocations(Point location)
        //{
        //    for (int i = 0; i < SkillContainers.Count; i++)
        //    {
        //        SkillContainers[i].UpdateLocation(location);
        //    }
        //}
    }

    public class PlayerContainer
    {
        public List<SkillIconContainer> SkillContainers = new List<SkillIconContainer>();
        private List<PictureBox> EmptySkillContainers = new List<PictureBox>();
        public int TimeLastSkillCast;
        public string CasterID;
        public int TotalDamageDone = 0;
        public int TotalHealingDone = 0;
        public int OverHealing = 0;
        public Label damageLabel;
        public int healLabelSpacer = 0;
        public Label healLabel;
        public Point Location;
        public Control parent;
        public int lastHealthKnown = 0;
        private static int HorizontalSkillOffset = 40;
        private PlayerRegister playerRegister;

        public PlayerContainer(string casterID, int timeLastSkillCast, Control parent, Point location, PlayerRegister playerRegister)
        {
            this.playerRegister = playerRegister;

            CasterID = casterID;
            TimeLastSkillCast = timeLastSkillCast;
            this.parent = parent;

            Location = location;
            damageLabel = new CustomLabel();
            damageLabel.TextAlign = ContentAlignment.BottomLeft;
            damageLabel.Width = 160;
            damageLabel.Text = "Damage: " + TotalDamageDone.ToString();
            damageLabel.Font = new Font("Arial", 12, FontStyle.Bold);
            //SkillName.AutoSize = true;
            damageLabel.Location = location;
            damageLabel.Parent = parent;

            CreateEmptySkillContainers();
            CreateHealLabel();
        }

        public void CreateHealLabel()
        {
            healLabel = new CustomLabel();
            healLabel.TextAlign = ContentAlignment.BottomLeft;
            healLabel.Width = 400;
            healLabel.Text = "Real Healing: " + (TotalDamageDone - OverHealing).ToString() + " | Over Healing " + OverHealing;
            healLabel.Font = new Font("Arial", 12, FontStyle.Bold);
            //SkillName.AutoSize = true;
            healLabel.Location = new Point(damageLabel.Location.X, damageLabel.Location.Y + 20);
            healLabel.Parent = parent;
            playerRegister.RepostionPlayerContainers();
        }

        public void UpdateHealLabel(int lastKnownHP, int healAmount, int newHP)
        {
            if (healLabel == null)
            {
                CreateHealLabel();
            }

            Console.WriteLine("LastKnownHP: " + lastKnownHP);

            if (lastKnownHP + healAmount >= newHP)
            {
                Console.WriteLine("OverHealed");
                OverHealing += lastKnownHP + healAmount - newHP;
            }

            TotalHealingDone += healAmount;

            healLabel.Text = "Actual Healing: " + (TotalHealingDone - OverHealing).ToString() + " | Over Healing " + OverHealing;

            //LastKnownHP
            //HealAmount
            //NewHp
            //
        }

        public void Reposition(Point pos)
        {
            Location = pos;
            damageLabel.Location = new Point(pos.X, pos.Y);
            if (healLabel != null)
            {
                healLabel.Location = new Point(damageLabel.Location.X, damageLabel.Location.Y + 20);
            }

            foreach (SkillIconContainer skillIconContainer in SkillContainers)
            {
                skillIconContainer.Reposition(new Point(Location.X + (int)skillIconContainer.Skill.SkillPosition * HorizontalSkillOffset, Location.Y));
            }

            for (int i = 0; i < 6; i++)
            {
                EmptySkillContainers[i].Location = new Point(Location.X + i * HorizontalSkillOffset, Location.Y + 40);
            }
        }

        public void CreateEmptySkillContainers()
        {
            for (int i = 0; i < 6; i++)
            {
                PictureBox pictureBox = new PictureBox();
                //pictureBox.ImageLocation = "./Resources/emptySkill.png";
                pictureBox.Image = SkillIDs.NAImage;
                pictureBox.Location = new Point(Location.X + i * HorizontalSkillOffset, Location.Y + 40);
                pictureBox.Size = new System.Drawing.Size(40, 40);
                pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                pictureBox.TabIndex = 0;
                pictureBox.TabStop = false;
                pictureBox.Parent = parent;
                EmptySkillContainers.Add(pictureBox);
            }
        }

        public void AddSkillContainer(string skillID, Skill skill)
        {
            Console.WriteLine("attempting to add skill to player: " + CasterID);
            bool exists = false;

            for (int i = SkillContainers.Count - 1; i >= 0; i --)
            {
                SkillIconContainer skillIconContainer = SkillContainers[i];

                if (skillIconContainer.Skill.ID == skillID)
                {
                    skillIconContainer.ResetCooldown();
                    exists = true;
                } else if (skillIconContainer.Skill.SkillPosition == skill.SkillPosition)
                {
                    skillIconContainer.Dispose();
                    SkillContainers.RemoveAt(i);
                    Console.WriteLine("Removed Occupied Skill Position");
                }
            }

            if (!exists)
            {
                Console.WriteLine("Added and hid skill at skill position: " + (int)skill.SkillPosition);
                EmptySkillContainers[(int)skill.SkillPosition].Hide();
                SkillContainers.Add(new SkillIconContainer(skill, parent, new Point(Location.X + (int)skill.SkillPosition * HorizontalSkillOffset, Location.Y)));

            }
        }

        public void Dispose()
        {
            parent.Controls.Remove(damageLabel);
            parent.Controls.Remove(healLabel);
            foreach (SkillIconContainer skillIconContainer in SkillContainers)
            {
                skillIconContainer.Dispose();
            }

            foreach (PictureBox pictureBox in EmptySkillContainers)
            {
                parent.Controls.Remove(pictureBox);
                pictureBox.Dispose();
            }
        }

        public void UpdateDamageLabel()
        {
            damageLabel.Text = "Damage: " + TotalDamageDone.ToString();
        }
    }

    public class SkillIconContainer
    {
        private PictureBox pictureBox;
        private Label timerLabel;
        //private Label skillNameLabel;

        public double CurrentCooldown;
        public Skill Skill;

        private static int pictureBoxVerticalOffset = 40;
        private static int timerLabelVerticalOffset = 80;
        //private static int skillNameLabelVerticalOffset = 20;

        public SkillIconContainer(Skill skill, Control parent, Point pos)
        {
            pictureBox = new PictureBox();
            pictureBox.Image = skill.SkillImage;
            pictureBox.Location = new Point(pos.X, pos.Y + pictureBoxVerticalOffset);
            pictureBox.Name = skill.Name;
            pictureBox.Size = new System.Drawing.Size(40, 40);
            pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            pictureBox.TabIndex = 0;
            pictureBox.TabStop = false;
            pictureBox.Parent = parent;

            Skill = skill;
            ResetCooldown();
            timerLabel = new CustomLabel();
            timerLabel.Location = new Point(pos.X, pos.Y + timerLabelVerticalOffset);
            timerLabel.Text = ((int)(CurrentCooldown / 1000)).ToString();
            timerLabel.Font = new Font("Arial", 16, FontStyle.Bold);
            timerLabel.AutoSize = true;
            timerLabel.Parent = parent;
            //skillNameLabel = new Label();
            //skillNameLabel.TextAlign = ContentAlignment.MiddleCenter;
            //skillNameLabel.Width = 130;
            //skillNameLabel.Text = skill.Name;
            //skillNameLabel.Font = new Font("Arial", 12, FontStyle.Bold);
            //SkillName.AutoSize = true;
            //skillNameLabel.Location = new System.Drawing.Point(pos.X - 60, pos.Y + skillNameLabelVerticalOffset);
            //skillNameLabel.Parent = parent;
        }

        public void Reposition(Point pos)
        {
            pictureBox.Location = new Point(pos.X, pos.Y + pictureBoxVerticalOffset);
            timerLabel.Location = new Point(pos.X - 5, pos.Y + timerLabelVerticalOffset);
        }

        public void ResetCooldown()
        {
            CurrentCooldown = Skill.Cooldown * 1000 + 500;//convert to milliseconds and add a half second buffer
        }

        public void Dispose()
        {
            pictureBox.Parent.Controls.Remove(pictureBox);
            pictureBox.Dispose();
            timerLabel.Parent.Controls.Remove(timerLabel);
            timerLabel.Dispose();
        }

        public void UpdateLabel(double timeSinceLastUpdate)
        {
            if (CurrentCooldown > 0)
            {
                CurrentCooldown -= timeSinceLastUpdate;
                timerLabel.Text = ((int)(CurrentCooldown / 1000)).ToString();
            }
        }
    }

    public class CustomLabel : Label
    {
        public CustomLabel()
        {
            OutlineForeColor = Color.White;
            OutlineWidth = 1;
        }
        public Color OutlineForeColor { get; set; }
        public float OutlineWidth { get; set; }
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(BackColor), ClientRectangle);
            using (GraphicsPath gp = new GraphicsPath())
            using (Pen outline = new Pen(OutlineForeColor, OutlineWidth)
            { LineJoin = LineJoin.Round })
            using (StringFormat sf = new StringFormat())
            using (Brush foreBrush = new SolidBrush(ForeColor))
            {
                gp.AddString(Text, Font.FontFamily, (int)Font.Style,
                    Font.Size, ClientRectangle, sf);
                e.Graphics.ScaleTransform(1.3f, 1.35f);
                e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
                e.Graphics.DrawPath(outline, gp);
                e.Graphics.FillPath(foreBrush, gp);
            }
        }
    }
}
