﻿using System;
using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace AlbionPacketParser
{
    public  class Program
    {
        // Create instance of the Userinterface
        //public static UserInterface UIForm ;

        //private IPhotonPackageHandler _photonHandler;
        //private PhotonPackageParser _photonPackageParser;
        public static Overlay GetOverlay = null;

      //static  UserInterface UI = new UserInterface();

        public static void Main(string[] args)
        {

            try
            {

                GetOverlay = new Overlay();
                Application.EnableVisualStyles();

                Application.Run(GetOverlay);
            }
            catch (Exception ex)
            {
                string filePath = "Error.txt";

                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                       "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                }
            }

            //Application.SetCompatibleTextRenderingDefault(false);
        }
    }
}
