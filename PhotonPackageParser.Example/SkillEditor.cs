﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbionPacketParser
{
    public class Skill
    {
        public string Name;
        public int Cooldown;
        public Image SkillImage;
        public string ID;
        public SkillPosition SkillPosition;

        public Skill(string name, int cooldown, string id, SkillPosition skillPosition)
        {
            Name = name;
            Cooldown = cooldown;
            string imageLocation = "./Resources/icons/" + BuildImageLocationString(name) + ".png";
            try
            {
                SkillImage = Image.FromFile(imageLocation);
            }
            catch
            {
                SkillImage = SkillIDs.NAImage;
                Console.WriteLine($"No Image Exists For Skill: {name}");
            }

            ID = id;
            SkillPosition = skillPosition;
        }

        private string BuildImageLocationString(string name)
        {
            string[] words = Name.ToLower().Split(' ');
            for (int i = 0; i < words.Count() - 1; i++)
            {
                string word = words[i];
                if (i == 0)
                {
                    continue;
                }

                word = char.ToUpper(word[0]) + word.Substring(1);
            }

            return String.Join("", words);
        }
    }

    public enum SkillPosition : int
    {
        WeaponOne = 0,
        WeaponTwo = 1,
        WeaponThree = 2,
        Chest = 3,
        Helmet = 4,
        Boots = 5,
        Potion = 6,
        Food = 7
    }

    public static class SkillIDs
    {
        public static readonly Image NAImage = Image.FromFile("./Resources/icons/emptySkill.png");
        public static SortedDictionary<string, Skill> SkillIDsDict;

        public static void SaveSkillsToFile()
        {
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter("SkillDat.txt"))
            {
                //Flush to completely overwrite existing file instead of appending
                file.Flush();

                foreach (KeyValuePair<string, Skill> skillID in SkillIDs.SkillIDsDict)
                {
                    int skillIDInt = Convert.ToInt32(skillID.Value.ID);

                    string line = $"{skillID.Value.Name}, {skillID.Value.Cooldown}, {skillIDInt}, {skillID.Value.SkillPosition}";
                    file.WriteLine(line);
                }
                file.Close();
            }
        }

        public static void LoadSkillsFromFile(string path = null)
        {
            SkillIDsDict = new SortedDictionary<string, Skill>();

            string[] lines;

            if (path == null)
            {
                lines = System.IO.File.ReadAllLines("SkillDat.txt");
            }
            else
            {
                lines = path.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }
            
            foreach (string line in lines)
            {
                string[] splitString = line.Split(',');
                if (splitString.Count() < 2)
                {
                    continue;
                }
                string name = splitString[0];
                Console.WriteLine(name);
                float cooldown = float.Parse(splitString[1].Replace(" ", string.Empty));
                Console.WriteLine(cooldown);
                string id = splitString[2].Replace(" ", string.Empty);
                               

                SkillPosition skillPosition;
                bool skillPositionParsed = Enum.TryParse(splitString[3], out skillPosition);

                if (!skillPositionParsed)
                {
                    Console.WriteLine("Skill Position Could Not Be Parsed, Skipping Line");
                    continue;
                }
                else
                {
                    SkillIDsDict.Add(id, new Skill(name, (int)cooldown, id, skillPosition));
                    //Console.WriteLine("\"" + id + "\"");
                }
            }

        }
    }
}
