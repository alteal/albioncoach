﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using PhotonPackageParser.Program;
using System.Threading;
using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;

namespace AlbionPacketParser
{
    //public partial class OverlayClickable : Form
    //{
    //    [DllImport("user32.dll")]
    //    static extern bool SetForegroundWindow(IntPtr hWnd);

    //    public OverlayClickable()
    //    {
    //        this.FormBorderStyle = FormBorderStyle.None;

    //        this.ShowInTaskbar = false;

    //        TransparencyKey = Color.Fuchsia;
    //        BackColor = TransparencyKey;

    //        TopMost = true;
    //    }

    //    private void InitializeComponent()
    //    {
    //        this.SuspendLayout();
    //        // 
    //        // OverlayClickable
    //        // 
    //        this.ClientSize = new System.Drawing.Size(284, 261);
    //        this.Name = "OverlayClickable";
    //        this.ResumeLayout(false);

    //    }
    //}

    public class MenuTool
    {
        private PictureBox menuIcon;
        private ContextMenu menuBox;
        private bool dragging;
        private bool pressed;
        private bool lockPosition;
        private int xPos;
        private int yPos;
        private Point Location;
        private Overlay parent;

        public MenuTool(Overlay parent)
        {
            menuBox = new ContextMenu();
            menuBox.MenuItems.Add("Clear Registers", ClearRegisters);
            menuBox.MenuItems.Add("Lock Repositioning", ToggleLockPositions);
            menuBox.MenuItems.Add("Device Selector", DeviceSelector);
            menuBox.MenuItems.Add("Exit", Exit);
            menuBox.MenuItems.Add("Load Update", LoadUpdate);

            //if debug add debug items
            //inserting caster
            //skillID edit menu
                //show last skill ID cast
                //change current skill ID and all subsequent IDs by x value
                //change only single skill ID and check and report conflicts
                //ask what to change each conflicted skill ID to new value

            //make report button for missing or invalid skill IDs

            menuIcon = new PictureBox();
            menuIcon.Image = Image.FromFile("Resources/menuIcon.png");
            menuIcon.Location = new Point(parent.Width - 40, parent.Height - 165 );
            menuIcon.Parent = parent;
            menuIcon.AutoSize = true;

            Location = menuIcon.Location;

            menuIcon.MouseMove += MouseMove;
            menuIcon.MouseUp += MouseUp;
            menuIcon.MouseDown += MouseDown;

            this.parent = parent;

            if (parent.Settings.LockPositions == true)
            {
                ToggleLockPositions(null, null);
            }
        }

        public void ToggleLockPositions(object sender, EventArgs e)
        {
            if (!lockPosition)
            {
                lockPosition = true;
                menuBox.MenuItems[1].Text = "Unlock Repositioning";
                
            } else
            {
                lockPosition = false;
                menuBox.MenuItems[1].Text = "Lock Repositioning";
            }

            parent.Settings.SaveLockPositions(lockPosition);

            parent.EnemyRegister.ToggleLockPositions();
            parent.FriendlyRegister.ToggleLockPositions();
        }

        public void ClearRegisters(object sender, EventArgs e)
        {
            parent.ResetAllCasters(sender, e);
        }

        public void Exit(object sender, EventArgs e)
        {
            if (parent.WorkerThread != null)
            {
                parent.WorkerThread.Abort();
            }
            Application.Exit();
        }

        public void LoadUpdate(object sender, EventArgs e)
        {
            SpellAnalyzer spellAnalyzer = new SpellAnalyzer();
            parent.SaveSkillData(sender, e);
        }

        public void DeviceSelector(object sender, EventArgs e)
        {
            parent.packetDeviceListBox.Show();
        }

        public void MouseUp(object sender, MouseEventArgs args)
        {
            var c = sender as PictureBox;
            if (null == c) return;
            if (dragging)
                dragging = false;
            else
                menuBox.Show(parent, Location);

            pressed = false;
        }

        public void MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;

            pressed = true;
            
            xPos = e.X;
            yPos = e.Y;
        }

        public void MouseMove(object sender, MouseEventArgs e)
        {
            if (!pressed || lockPosition)
                return;

            dragging = true;
            var c = sender as PictureBox;
            if (!dragging || null == c) return;
            c.Top = e.Y + c.Top - yPos;
            c.Left = e.X + c.Left - xPos;
            Location.X = c.Left + 5;
            Location.Y = c.Top + 25;
        }
    }

    public partial class Overlay : Form
    {
        private IPhotonPackageHandler _photonHandler;
        private PhotonPackageParser.Program.PhotonPackageParser _photonPackageParser;

        private double totalTimeElapsed = 0; //in milliseconds
        private double lastSignalTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        private double timeElapsed = 0;

        public Point FriendlyTagPosition = new Point(200, 200);
        public Point EnemyTagPosition = new Point(1366, 200);
        public int VerticalSkillIconOffset = 110;
        public int HorizontalPlayerRegisterOffset = 200;

        public PlayerRegister EnemyRegister;
        public PlayerRegister FriendlyRegister;

        public Settings Settings;

        private PacketDevice packetDevice;

        public ListBox packetDeviceListBox;

        private MenuTool menuTool;

        private System.Timers.Timer timer = new System.Timers.Timer(500);

        public Thread WorkerThread;

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        //[DllImport("user32.dll", SetLastError = true)]
        //static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        //[DllImport("user32.dll")]
        //static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        //const int GWL_EXSTYLE = -20;
        //const int WS_EX_LAYERED = 0x80000;
        //const int WS_EX_TRANSPARENT = 0x20;

        public Overlay()
        {
            SkillIDs.LoadSkillsFromFile();

            Settings = new Settings();
            Settings.LoadSettings();

            this.FormBorderStyle = FormBorderStyle.None;

            // make the form clickthrough..
            TransparencyKey = Color.Gray;
            BackColor = TransparencyKey;

            TopMost = true;

            InitializeComponent();

            timer.Elapsed += timer1_Tick;
            timer.Enabled = true;

            EnemyRegister = new PlayerRegister(new Point(this.Width - 400, 80), this, Settings.SaveEnemyPlayerContainerPosition);

            if (Settings.EnemyPlayerContainerPosition == null)
            {
                Settings.EnemyPlayerContainerPosition = EnemyRegister.Location;
            }
            else
            {
                EnemyRegister.Location = (Point)Settings.EnemyPlayerContainerPosition;
                EnemyRegister.MoveIcon.Location = new Point(EnemyRegister.Location.X - 10, EnemyRegister.Location.Y - 25);
                EnemyRegister.RepostionPlayerContainers();
            }

            FriendlyRegister = new PlayerRegister(new Point(200, 80), this, Settings.SaveFriendlyPlayerContainerPosition);

            if (Settings.FriendlyPlayerContainerPosition == null)
            {
                Settings.FriendlyPlayerContainerPosition = FriendlyRegister.Location;
            }
            else
            {
                FriendlyRegister.Location = (Point)Settings.FriendlyPlayerContainerPosition;
                FriendlyRegister.MoveIcon.Location = new Point(FriendlyRegister.Location.X - 10, FriendlyRegister.Location.Y - 25);
                FriendlyRegister.RepostionPlayerContainers();
            }

            EnemyRegister.UseOppositeRegister(FriendlyRegister, SwapContainerInRegister);
            FriendlyRegister.UseOppositeRegister(EnemyRegister, SwapContainerInRegister);

            packetDeviceListBox = new ListBox();
            packetDeviceListBox.AutoSize = true;
            packetDeviceListBox.Location = new Point(80, 110);
            packetDeviceListBox.Parent = this;
            packetDeviceListBox.BackColor = Color.White;
            packetDeviceListBox.MouseDoubleClick += SetPacketDevice;

            menuTool = new MenuTool(this);

            //menuBox.Show(this, new Point(this.Width - 200, this.Height - 200));

            DeviceSelector.DeterminePacketDevices();
            LoadDeviceListEntries();

            if (Settings.NetworkDeviceIndex != null)
            {
                packetDevice = DeviceSelector.AllDevices[(int)Settings.NetworkDeviceIndex];
                if (WorkerThread != null)
                {
                    WorkerThread.Abort();
                }
                packetDeviceListBox.Hide();
                LaunchSnifferThread();
            }
        }

        public void LoadDeviceListEntries()
        {
            for (int i = 0; i != DeviceSelector.AllDevices.Count; ++i)
            {
                string entry = "";
                LivePacketDevice device = DeviceSelector.AllDevices[i];
                entry += (i + 1) + ". ";
                entry += (" " + string.Join(",",
                                  device.Addresses.Where(x => x.Address.Family == SocketAddressFamily.Internet)
                                      .Select(x => x.Address.ToString()).ToArray()) + " ");
                if (device.Description != null)
                    entry += (" (" + device.Description + ")");
                else
                    entry += (" (No description available)");
                packetDeviceListBox.Items.Add(entry);
            }
        }

        public void SetPacketDevice(object sender, EventArgs e)
        {
            if (Settings.NetworkDeviceIndex != packetDeviceListBox.SelectedIndex)
            {
                Settings.SaveNetworkDeviceIndex(packetDeviceListBox.SelectedIndex);
            }
            packetDevice = DeviceSelector.AllDevices[packetDeviceListBox.SelectedIndex];
            Console.WriteLine("Selected Device:  " + packetDeviceListBox.SelectedItem);
            if (WorkerThread != null)
            {
                WorkerThread.Abort();
            }
            packetDeviceListBox.Hide();
            LaunchSnifferThread();
        }

        public void SaveSkillData(object sender, EventArgs e)
        {
            SkillIDs.SaveSkillsToFile();
        }
        
        public void SwapContainerInRegister(PlayerRegister from , PlayerRegister to, PlayerContainer playerContainer)
        {
            from.playerContainers.Remove(playerContainer);
            to.playerContainers.Add(playerContainer);
        }

        public void ResetAllCasters(object sender, EventArgs e)
        {
            EnemyRegister.Dispose();
            FriendlyRegister.Dispose();
        }

        public void RandomFriendlyCaster(object sender, EventArgs e)
        {
            Random random = new Random();
            RegisterFriendlyCaster(random.Next(0, 9999).ToString());
        }

        public void RegisterFriendlyCaster(string casterID)
        {
            int i;

            if (FriendlyRegister.ExistsInPlayerContainer(casterID, out i))
            {
                return;
            }

            else if (EnemyRegister.ExistsInPlayerContainer(casterID, out i))
            {
                Console.WriteLine("Moving Friendly Caster");
                FriendlyRegister.playerContainers.Add(EnemyRegister.playerContainers[i]);
                EnemyRegister.playerContainers.RemoveAt(i);
                Console.WriteLine("Moved Control Caster to Friendly");

                Controls.Remove(EnemyRegister.swapButtons[i]);
                EnemyRegister.swapButtons[i].Dispose();
                EnemyRegister.swapButtons.RemoveAt(i);
                FriendlyRegister.AddSwapButton(FriendlyRegister.playerContainers.Last());
                Console.WriteLine("Moved swap buttons");

                EnemyRegister.RepostionPlayerContainers();
                FriendlyRegister.RepostionPlayerContainers();
            }
            else
            {
                FriendlyRegister.AddPlayerContainer(casterID, (int)totalTimeElapsed);
                FriendlyRegister.RepostionPlayerContainers();
            }
        }

        public PlayerContainer GetPlayerOrRegisterEnemy(string playerID)
        {
            int i;
            if (FriendlyRegister.ExistsInPlayerContainer(playerID, out i))
            {
                return FriendlyRegister.playerContainers[i];
            } else if (EnemyRegister.ExistsInPlayerContainer(playerID, out i))
            {
                return EnemyRegister.playerContainers[i];
            } else
            {
                EnemyRegister.AddPlayerContainer(playerID, (int)totalTimeElapsed);
                return EnemyRegister.playerContainers[EnemyRegister.playerContainers.Count - 1];
            }
        }

        public int LastKnownPlayerHP(string targetID)
        {
            foreach (PlayerContainer playerContainer in EnemyRegister.playerContainers)
            {
                if (playerContainer.CasterID == targetID)
                {
                    return playerContainer.lastHealthKnown;
                }
            }
            foreach (PlayerContainer playerContainer in FriendlyRegister.playerContainers)
            {
                if (playerContainer.CasterID == targetID)
                {
                    return playerContainer.lastHealthKnown;
                }
            }

            return 0;
        }

        public void Debug(string type, int Eventcode, int Operationcode, Dictionary<byte, object> parameters)
        {
            bool giveUp = true;

            if (EventCodes.EventToString[Eventcode] == "HealthUpdate")
            {
                string casterID = parameters[(byte)6].ToString();
                string targetID = parameters[(byte)0].ToString();
                int newHP = Convert.ToInt32(parameters[(byte)3].ToString());
                int damage = Convert.ToInt32(parameters[(byte)2].ToString());

                if (damage < 0)
                {
                    foreach (PlayerContainer playerContainer in EnemyRegister.playerContainers)
                    {
                        if (playerContainer.CasterID == targetID)
                        {
                            playerContainer.lastHealthKnown = newHP;
                        }
                        if (playerContainer.CasterID == casterID)
                        {
                            playerContainer.TotalDamageDone += (int)Math.Abs(damage);
                            playerContainer.UpdateDamageLabel();
                        }
                    }
                    foreach (PlayerContainer playerContainer in FriendlyRegister.playerContainers)
                    {
                        if (playerContainer.CasterID == targetID)
                        {
                            playerContainer.lastHealthKnown = newHP;
                        }
                        if (playerContainer.CasterID == casterID)
                        {
                            playerContainer.TotalDamageDone += (int)Math.Abs(damage);
                            playerContainer.UpdateDamageLabel();
                        }
                    }
                } else
                {
                    //Event: HealthUpdate  6
                    //Key = 0, Value = 322
                    //Key = 1, Value = 60636260
                    //Key = 2, Value = 321
                    //Key = 3, Value = 1000
                    //Key = 4, Value = 2
                    //Key = 5, Value = 2
                    //Key = 6, Value = 322
                    //Key = 7, Value = 1282
                    //Key = 252, Value = 6

                    if (FriendlyRegister.ExistsInPlayerContainer(casterID))
                    {
                        RegisterFriendlyCaster(targetID);
                    }

                    int healAmount = Convert.ToInt32(parameters[(byte)2].ToString());
                    int lastKnownHP = LastKnownPlayerHP(targetID);
                    foreach (PlayerContainer playerContainer in EnemyRegister.playerContainers)
                    {
                        if (playerContainer.CasterID == casterID)
                        {
                            playerContainer.UpdateHealLabel(lastKnownHP, healAmount, newHP);
                        }

                        if (playerContainer.CasterID == targetID)
                        {
                            playerContainer.lastHealthKnown = newHP;
                        }
                    }
                    foreach (PlayerContainer playerContainer in FriendlyRegister.playerContainers)
                    {
                        if (playerContainer.CasterID == casterID)
                        {
                            playerContainer.UpdateHealLabel(lastKnownHP, healAmount, newHP);
                        }

                        if (playerContainer.CasterID == targetID)
                        {
                            playerContainer.lastHealthKnown = newHP;
                        }
                    }
                }
            }

            if (EventCodes.EventToString[Eventcode] == "EnergyUpdate")
            {
                if (parameters.ContainsKey((byte)6))
                {
                    //int skillIDInt = Convert.ToInt32(parameters[(byte)6].ToString());
                    string skillID = parameters[(byte)6].ToString();

                    string casterID = parameters[(byte)5].ToString();

                    Console.WriteLine("ContainsSkill");
                    Console.WriteLine("Skill ID: " + skillID);

                    //RegisterControlCaster(casterID, skillID);

                    if (SkillIDs.SkillIDsDict.ContainsKey(skillID))
                    {
                        PlayerContainer player = GetPlayerOrRegisterEnemy(casterID);
                        Skill skill = SkillIDs.SkillIDsDict[skillID];
                        player.AddSkillContainer(skillID, skill);
                    //    PlayerRegister enemyRegister = RegisterEnemy(casterID, timeElapsed);
                    //    Skill skill = SkillIDs.SkillIDsDict[skillID];
                    //    RegisterEnemySkill(enemyRegister, skillID, skill);
                    }
                }
                giveUp = false;
            }

            //if (giveUp)
            //    return;

            return;


            //if (Eventcode > 0 && Operationcode == 0) { alphaBlendTextBox1.AppendText("Event:" + EventCodes.EventToString[Eventcode] + "  " + Eventcode + Environment.NewLine); }
            //else if (Operationcode > 0 && Eventcode == 0) { alphaBlendTextBox1.AppendText(type + ": " + OperationCodes.OperationToString[Operationcode] + "  " + Operationcode + Environment.NewLine); }

            ////if (saved == false) { saved = true; } else { return; }
            ////FileStream writerFileStream =
            ////     new FileStream("TestFile.dat", FileMode.Create, FileAccess.Write);
            //// Save our dictionary of friends to file
            ////formatter.Serialize(writerFileStream, parameters);
            ////MessageBox.Show("Im still working");
            //foreach (KeyValuePair<byte, object> kvp in parameters)
            //{
            //    ///try
            //    //{

            //    // StringArray
            //    if (kvp.Value is string[] StringArray) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < StringArray.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + StringArray[i] + Environment.NewLine); } }

            //    // Guid
            //    //else if (kvp.Value is Guid _Guid) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); alphaBlendTextBox1.AppendText(_Guid.ToString() + Environment.NewLine); }

            //    // Guid Array
            //    else if (kvp.Value is Guid[] _GuidArray) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < _GuidArray.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + _GuidArray[i].ToString() + Environment.NewLine); } }

            //    // Int32 Array
            //    else if (kvp.Value is Int16[] Int16Array1) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int16Array1.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + Int16Array1[i] + Environment.NewLine); } }
            //    else if (kvp.Value is Int16[][] Int16Array2) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int16Array2.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + Int16Array2[i] + Environment.NewLine); } }


            //    // Int32 Array
            //    else if (kvp.Value is Int32[] Int32Array1) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int32Array1.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + Int32Array1[i] + Environment.NewLine); } }
            //    else if (kvp.Value is Int32[][] Int32Array2) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int32Array2.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + Int32Array2[i] + Environment.NewLine); } }

            //    // Int64 Array
            //    else if (kvp.Value is Int64[] Int64Array1) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int64Array1.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + Int64Array1[i] + Environment.NewLine); } }
            //    else if (kvp.Value is Int64[][] Int64Array2) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < Int64Array2.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + Int64Array2[i] + Environment.NewLine); } }

            //    // Most of the time Byte[] and Byte[][] are GUID and GUID arrays.
            //    // Sometimes they are not. This piece checks if it is possibly a GUID, if yes then it casts it as a GUID and continues with it.
            //    // If not, it will write the bytes withing the array.
            //    // Byte Array 
            //    else if (kvp.Value is Byte[] ByteArray1) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); if (ByteArray1.Length == 16) { Guid _guid = new Guid(ByteArray1); alphaBlendTextBox1.AppendText(_guid.ToString() + Environment.NewLine); } else { for (int i = 0; i < ByteArray1.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + ByteArray1[i] + Environment.NewLine); } } }
            //    else if (kvp.Value is byte[][] ByteArray2) { if (ByteArray2.Length == 0) { return; } alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); if (ByteArray2[0].Length == 16) { List<byte[]> GuidList = ByteArray2.Cast<byte[]>().ToList(); for (int i = 0; i < GuidList.Count; i++) { Guid _Guid = new Guid(GuidList[i]); alphaBlendTextBox1.AppendText(_Guid.ToString() + Environment.NewLine); } } else { if (ByteArray2.Length == 0 || ByteArray2[0].Length == 0) { alphaBlendTextBox1.AppendText("Empty Byte[][]" + Environment.NewLine); goto SKIP; } else { for (int i = 0; i < ByteArray2.Length; i++) { alphaBlendTextBox1.AppendText(i + "  "); for (int j = 0; j < ByteArray2[i].Length; j++) { alphaBlendTextBox1.AppendText(ByteArray2[i][j].ToString()); } alphaBlendTextBox1.AppendText(Environment.NewLine); } } } } // ByteArray2[i]

            //    // Single Array
            //    else if (kvp.Value is Single[] SingleArray1) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < SingleArray1.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + SingleArray1[i] + Environment.NewLine); } }
            //    else if (kvp.Value is Single[][] SingleArray2) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); for (int i = 0; i < SingleArray2.Length; i++) { alphaBlendTextBox1.AppendText(i + " : " + SingleArray2[i] + Environment.NewLine); } }

            //    // Dictionary
            //    else if (kvp.Value is Dictionary<byte, byte> Dict) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, byte> _kvp in Dict) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
            //    else if (kvp.Value is Dictionary<byte, string> Dict_Byte_String) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, string> _kvp in Dict_Byte_String) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
            //    else if (kvp.Value is Dictionary<byte, Int32> Dict_Byte_Int32) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, Int32> _kvp in Dict_Byte_Int32) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
            //    else if (kvp.Value is Dictionary<byte, Int64> Dict_byte_Int64) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<byte, Int64> _kvp in Dict_byte_Int64) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
            //    else if (kvp.Value is Dictionary<Int32, Int64> Dict_Int32_Int64) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); foreach (KeyValuePair<Int32, Int64> _kvp in Dict_Int32_Int64) { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); } }
            //    else { alphaBlendTextBox1.AppendText("Key = " + kvp.Key + ", Value = " + kvp.Value + Environment.NewLine); }

            //    //}
            //    //catch (Exception E) { MessageBox.Show("ii = "+ii+Environment.NewLine +"jj = "+jj+Environment.NewLine + E.Message + Environment.NewLine + len1 + "    "+len2); }
            //    SKIP:;
            //}


        }

        private void Overlay_Load(object sender, EventArgs e)
        {
            //var style = GetWindowLong(this.Handle, GWL_EXSTYLE);
            //SetWindowLong(this.Handle, GWL_EXSTYLE, style | WS_EX_LAYERED | WS_EX_TRANSPARENT);
            //LaunchSnifferThread();
        }

        private void LaunchSnifferThread()
        {
            WorkerThread = new Thread(() => Start());
            WorkerThread.IsBackground = true;
            WorkerThread.Start();
        }

        private void Start()
        {
            var device = packetDevice; // get Network Device

            _photonHandler = new PackageHandler();
            _photonPackageParser = new PhotonPackageParser.Program.PhotonPackageParser(_photonHandler);

            using (PacketCommunicator communicator = device.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 1000))
            {
                communicator.ReceivePackets(0, PacketHandler); // Initiate the never ending listener
            }
        }

        private void PacketHandler(Packet packet) // Callback function when A packet is received.
        {
            IpV4Datagram ip = packet.Ethernet.IpV4;
            UdpDatagram udp = ip.Udp;

            if (udp == null || (udp.SourcePort != 5056 && udp.DestinationPort != 5056))
            // Checking if the packet is valid and comes from the right ports.
            {
                return;
            }

            _photonPackageParser.DeserializeMessageAndCallback(udp); // Give the UDP datagram to the deserialiser.
        }

        private void timer1_Tick(Object source, System.Timers.ElapsedEventArgs e)
        {
            timeElapsed = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - lastSignalTime;
            totalTimeElapsed += timeElapsed;
            lastSignalTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            foreach (PlayerContainer playerContainer in EnemyRegister.playerContainers)
            {
                playerContainer.UpdateDamageLabel();
                foreach (SkillIconContainer skillIconContainer in playerContainer.SkillContainers)
                {
                    this.BeginInvoke(new MethodInvoker(delegate
                    {
                        skillIconContainer.UpdateLabel(timeElapsed);
                    }));
                }
            }

            foreach (PlayerContainer playerContainer in FriendlyRegister.playerContainers)
            {
                playerContainer.UpdateDamageLabel();
                foreach (SkillIconContainer skillIconContainer in playerContainer.SkillContainers)
                {
                    this.BeginInvoke(new MethodInvoker(delegate
                    {
                        skillIconContainer.UpdateLabel(timeElapsed);
                    }));
                }
            }
        }
    }
}
